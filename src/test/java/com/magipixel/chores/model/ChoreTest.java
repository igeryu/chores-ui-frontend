package com.magipixel.chores.model;

import com.magipixel.chores.MockitoStrictStubbingTest;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.util.HashMap;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

@Tag("unit")
class ChoreTest extends MockitoStrictStubbingTest {
    @InjectMocks
    private Chore chore;


    @Test
    void unpackIdFromLinks() {
        // Arrange
        final long expectedId = 123L;
        final HashMap<String, Object> links = new HashMap<String, Object>() {{
            final HashMap<String, Object> self = new HashMap<String, Object>() {{
                put("href", "abcd/this/is/a/uri/" + expectedId);
            }};
            put("self", self);
        }};

        // Act
        chore.unpackIdFromLinks(links);

        // Assert
        assertThat(chore.getId(), is(equalTo(expectedId)));
    }

}