package com.magipixel.chores.model;

import com.magipixel.chores.MockitoStrictStubbingTest;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

@Tag("unit")
class ChoreParentTest extends MockitoStrictStubbingTest {
    @InjectMocks
    private ChoreParent choreParent;

    @Test
    void getParentId() {
        // Arrange
        final long expectedParentId = 456L;

        // Act
        choreParent.setParentId(expectedParentId);

        // Assert
        assertThat(choreParent.getParentId(), is(equalTo(expectedParentId)));
    }

}