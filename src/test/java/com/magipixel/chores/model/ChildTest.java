package com.magipixel.chores.model;

import com.magipixel.chores.MockitoStrictStubbingTest;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.util.HashMap;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

@Tag("unit")
class ChildTest extends MockitoStrictStubbingTest {
    @InjectMocks
    private Child child;

    @Test
    void getFirstName() {
        // Arrange
        final String expectedFirstName = "expected_firstName";

        // Act
        child.setFirstName(expectedFirstName);

        // Assert
        assertThat(child.getFirstName(), is(equalTo(expectedFirstName)));
    }

    @Test
    void unpackIdFromLinks() {
        // Arrange
        final long expectedId = 123L;
        final HashMap<String, Object> links = new HashMap<String, Object>() {{
            final HashMap<String, Object> self = new HashMap<String, Object>() {{
                put("href", "abcd/this/is/a/uri/" + expectedId);
            }};
            put("self", self);
        }};

        // Act
        child.unpackIdFromLinks(links);

        // Assert
        assertThat(child.getId(), is(equalTo(expectedId)));
    }
}