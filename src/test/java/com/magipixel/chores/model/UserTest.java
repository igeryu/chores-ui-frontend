package com.magipixel.chores.model;

import com.magipixel.chores.MockitoStrictStubbingTest;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

@Tag("unit")
class UserTest extends MockitoStrictStubbingTest {
    @InjectMocks
    private User user;

    @Test
    void getPassword() {
        // Arrange
        final String expectedPassword = "expected_password";

        // Act
        user.setPassword(expectedPassword);

        // Assert
        assertThat(user.getPassword(), is(equalTo(expectedPassword)));
    }

    @Test
    void getRole() {
        // Arrange
        final Role expectedRole = Role.PARENT;

        // Act
        user.setRole(expectedRole);

        // Assert
        assertThat(user.getRole(), is(equalTo(expectedRole)));
    }

    @Test
    @SuppressWarnings("unchecked")
    void givenLinksWhenUnpackIdFromLinksThenIdIsSet() {
        final Map<String, Object> links = new HashMap() {{
            final Map<String, String> self = new HashMap() {{
                put("href", "/uri/blah/with/blah/id/123");
            }};
            put("self", self);
        }};

        user.unpackIdFromLinks(links);

        // Assert
        assertThat(user.getId(), is(equalTo(123L)));
    }
}