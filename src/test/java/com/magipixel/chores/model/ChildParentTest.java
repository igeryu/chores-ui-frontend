package com.magipixel.chores.model;

import com.magipixel.chores.MockitoStrictStubbingTest;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

@Tag("unit")
class ChildParentTest extends MockitoStrictStubbingTest {
    @InjectMocks
    private ChildParent childParent;

    @Test
    void getParentId() {
        // Arrange
        final long expectedParentId = 456L;

        // Act
        childParent.setParentId(expectedParentId);

        // Assert
        assertThat(childParent.getParentId(), is(equalTo(expectedParentId)));
    }

}