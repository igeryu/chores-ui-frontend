package com.magipixel.chores.hamcrest;

import lombok.RequiredArgsConstructor;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.util.Optional;

@RequiredArgsConstructor
public class OptionalMatcher extends TypeSafeMatcher<Optional> {
    private final boolean isPresent;

    @SuppressWarnings("OptionalAssignedToNull")
    @Override
    protected boolean matchesSafely(final Optional optional) {
        return optional != null && optional.isPresent() == this.isPresent;
    }

    @Override
    public void describeTo(final Description description) {
    }

    public static Matcher<Optional> present() {
        return new OptionalMatcher(true);
    }

}
