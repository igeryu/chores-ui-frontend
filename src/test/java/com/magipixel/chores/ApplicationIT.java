package com.magipixel.chores;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;

@SpringBootTest(classes = Application.class)
@AutoConfigureWireMock(port = 0)
@Tag("integration")
class ApplicationIT {

    @Test
    void contextLoads() {
    }

}