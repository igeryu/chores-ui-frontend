package com.magipixel.chores;

import com.magipixel.chores.controller.ChildController;
import com.magipixel.chores.service.ChildService;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest({
        ChildController.class,
        ChildService.class
})
@Tag("integration")
class ChildSecurityIT {

    @MockBean
    private ChildService service;

    @Autowired
    private ChildController controller;

    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Nested
    @DisplayName("Create Child")
    class CreateChild {

        @Test
        @WithMockUser(roles = "PARENT")
        @DisplayName("Given User is Parent, createChild is mapped and succeeds")
        void givenUserIsParent_createChild_isMapped_andSucceeds() throws Exception {
            // Act / Assert
            mockMvc.perform(post("/children")
                    .header("Authorization", "Bearer can_be_anything")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{}"))
                    .andDo(print())
                    .andExpect(status()
                            .isCreated());
        }

        @Test
        @WithMockUser(roles = "CHILD")
        @DisplayName("Given User is authenticated but not a Parent, createChild returns 401 Unauthorized")
        void givenUserIsNotAuthenticatedButNotParent_createChild_returnsUnauthorized() throws Exception {
            // Act / Assert
            mockMvc.perform(post("/children")
                    .header("Authorization", "Bearer can_be_anything")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{}"))
                    .andDo(print())
                    .andExpect(status()
                            .isForbidden());
        }

        @Test
        @DisplayName("Given no Authorization header, createChild returns 401 Unauthorized")
        void givenNoAuthorizationHeader_createChild_returns401NotUnauthorized() throws Exception {
            // Arrange
            final String content =
                    IOUtils.toString(this.getClass().getResourceAsStream("/json/bb-child-crud/New_Child_Request.json"));

            // Act / Assert
            mockMvc.perform(post("/children")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(content))
                    .andDo(print())
                    .andExpect(status()
                            .isUnauthorized());
        }

    }

    @Nested
    @DisplayName("Get All Children")
    class GetAllChildren {

        @Test
        @WithMockUser(roles = "PARENT")
        @DisplayName("Given User is Parent, getAllChildren is mapped and succeeds")
        void givenUserIsParent_getAllChildren_isMapped_andSucceeds() throws Exception {
            // Act / Assert
            mockMvc.perform(get("/children")
                    .header("Authorization", "Bearer can_be_anything")
                    .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status()
                            .isOk())
                    .andDo(print());
        }

        @Test
        @WithMockUser(roles = "CHILD")
        @DisplayName("Given User is authenticated but not a Parent, getAllChildren returns 401 Unauthorized")
        void givenUserIsAuthenticatedButNotParent_getAllChildren_returnsUnauthorized() throws Exception {
            // Act / Assert
            mockMvc.perform(get("/children")
                    .header("Authorization", "Bearer can_be_anything")
                    .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status()
                            .isForbidden())
                    .andDo(print());
        }

        @Test
        @DisplayName("Given no Authorization header, getAllChildren returns 401 Unauthorized")
        void givenNoAuthorizationHeader_getAllChildren_returns401NotUnauthorized() throws Exception {
            // Act / Assert
            mockMvc.perform(get("/children")
                    .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status()
                            .isUnauthorized())
                    .andDo(print());
        }

    }

    @Nested
    @DisplayName("Get Child")
    class GetChild {

        @Test
        @DisplayName("Given no Authorization header, getChild returns 401 Unauthorized")
        void givenNoAuthorizationHeader_getChild_returns401NotUnauthorized() throws Exception {
            // Act / Assert
            mockMvc.perform(get("/children/567")
                    .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status()
                            .isUnauthorized())
                    .andDo(print());
        }

    }
}