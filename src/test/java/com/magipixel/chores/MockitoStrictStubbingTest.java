package com.magipixel.chores;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.mockito.quality.Strictness;

public abstract class MockitoStrictStubbingTest {
    private MockitoSession mockito;

    @BeforeEach
    public void setUpStrictMocking() {
        // initialize session to start mocking
        mockito = Mockito.mockitoSession()
                .initMocks(this)
                .strictness(Strictness.STRICT_STUBS)
                .startMocking();
    }

    @AfterEach
    public void validateStrictMocking() {
        // It is necessary to finish the session so that Mockito
        // can detect incorrect stubbing and validate Mockito usage
        mockito.finishMocking();
    }

}
