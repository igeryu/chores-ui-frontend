package com.magipixel.chores;

import com.magipixel.chores.controller.CredentialsController;
import com.magipixel.chores.service.CredentialsService;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest({CredentialsController.class, CredentialsService.class})
@Tag("integration")
class CredentialsSecurityIT {

    @MockBean
    private CredentialsService service;

    @Autowired
    private CredentialsController controller;

    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    @DisplayName("Given User is not authenticated, createCredentials succeeds")
    void givenUserIsNotAuthenticated_createCredentials_succeeds() throws Exception {
        // Arrange
        final String content = IOUtils
                .toString(this.getClass().getResourceAsStream("/json/bb-credentials/New_Credentials_Request.json"));

        // Act / Assert
        mockMvc.perform(post("/credentials/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andDo(print())
                .andExpect(status()
                        .isCreated());
    }

    @Test
    @WithMockUser(roles = "PARENT")
    @DisplayName("Given User is authenticated as Parent, createCredentials succeeds")
    void givenUserIsAuthenticated_createCredentials_succeeds() throws Exception {
        // Arrange
        final String content = IOUtils
                .toString(this.getClass().getResourceAsStream("/json/bb-credentials/New_Credentials_Request.json"));

        // Act / Assert
        mockMvc.perform(post("/credentials/users")
                .header("Authorization", "Bearer can_be_anything")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andDo(print())
                .andExpect(status()
                        .isCreated());
    }
}