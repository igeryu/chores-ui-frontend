package com.magipixel.chores.service;

import com.magipixel.chores.MockitoStrictStubbingTest;
import com.magipixel.chores.client.CredentialsClient;
import com.magipixel.chores.exception.DuplicateAccountException;
import com.magipixel.chores.model.Role;
import com.magipixel.chores.model.User;
import feign.FeignException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

@Tag("unit")
class CredentialsServiceTest extends MockitoStrictStubbingTest {
    @InjectMocks
    private CredentialsService service;

    @Mock
    private CredentialsClient client;

    @Test
    @DisplayName("createCredentials calls client, and adds Parent role")
    void createCredentials_callsClient_andAddsParentRole() throws DuplicateAccountException {
        // Arrange
        final User input = new User();
        final String expectedUsername = "expected_username";
        input.setUsername(expectedUsername);
        final String expectedPassword = "expected_password";
        input.setPassword(expectedPassword);

        // Act
        service.createCredentials(input);

        // Assert
        final User expected = new User();
        expected.setUsername(expectedUsername);
        expected.setPassword(expectedPassword);
        expected.setRole(Role.PARENT);
        then(client).should().createCredentials(expected);
    }

    @Test
    @DisplayName("createCredentials calls client, and adds Parent role")
    void createCredentials_givenFeignException_Conflict_returnsDuplicateAccountException() {
        // Arrange
        final User input = new User();
        final String expectedUsername = "expected_username";
        input.setUsername(expectedUsername);
        final String expectedPassword = "expected_password";
        input.setPassword(expectedPassword);
        final FeignException feignException = mock(FeignException.Conflict.class);
        doThrow(feignException).when(client).createCredentials(any(User.class));

        // Act / Assert
        final DuplicateAccountException duplicateAccountException =
                assertThrows(DuplicateAccountException.class, () -> service.createCredentials(input));

        assertThat(duplicateAccountException.getMessage(), equalTo("Account already exists: " + expectedUsername));
    }
}