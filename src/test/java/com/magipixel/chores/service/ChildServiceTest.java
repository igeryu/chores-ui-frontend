package com.magipixel.chores.service;

import com.magipixel.chores.MockitoStrictStubbingTest;
import com.magipixel.chores.client.ChildClient;
import com.magipixel.chores.client.ChildParentClient;
import com.magipixel.chores.client.ChoreChildClient;
import com.magipixel.chores.client.ChoreClient;
import com.magipixel.chores.client.CredentialsClient;
import com.magipixel.chores.exception.DuplicateChildException;
import com.magipixel.chores.exception.EntityNotFoundException;
import com.magipixel.chores.model.Child;
import com.magipixel.chores.model.ChildParent;
import com.magipixel.chores.model.Chore;
import com.magipixel.chores.model.ChoreChild;
import com.magipixel.chores.model.User;
import feign.FeignException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.hateoas.CollectionModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

@Tag("unit")
class ChildServiceTest extends MockitoStrictStubbingTest {
    @Mock
    private CredentialsClient credentialsClient;

    @Mock
    private ChildClient childClient;

    @Mock
    private ChildParentClient childParentClient;

    @Mock
    private ChoreChildClient choreChildClient;

    @Mock
    private ChoreClient choreClient;

    @InjectMocks
    private ChildService service;

    @Nested
    @DisplayName("createChild")
    @Tag("Create_Child")
    class CreateChild {

        @Test
        @DisplayName("happy path")
        void happyPath() throws DuplicateChildException {
            // Arrange
            //    Look up Parent ID
            final String expectedToken = "expected_token";
            final String parentEmail = "parent_email";
            final User parent = new User();
            final long expectedParentId = 456L;
            parent.setId(expectedParentId);
            given(credentialsClient.findByEmail(expectedToken, parentEmail)).willReturn(Optional.of(parent));

            //    Create new Child entity
            final Child childToCreate = new Child();
            childToCreate.setFirstName("1234_first_name]");
            childToCreate.setEmail("some_email");
            final Child createdChild = new Child();
            final long expectedChildId = 123L;
            createdChild.setId(expectedChildId);
            given(childClient.createChild(expectedToken, childToCreate)).willReturn(createdChild);

            // Act
            service.createChild(expectedToken, parentEmail, childToCreate);

            // Assert
            //    Associate new Child to Parent
            final ChildParent expectedChild = new ChildParent();
            expectedChild.setParentId(expectedParentId);
            expectedChild.setChildId(expectedChildId);
            then(childParentClient).should().createChildParentLink(expectedToken, expectedChild);
        }

        @Test
        @DisplayName("given Parent does not exist, should return 'entity not found'")
        void givenParentDoesNotExist_shouldReturnEntityNotFound() {
            // Arrange
            //    Look up Parent ID
            given(credentialsClient.findByEmail(any(), any())).willReturn(Optional.empty());

            final Child input = new Child();
            input.setEmail("email");

            // Act / Assert
            assertThrows(EntityNotFoundException.class, () -> service.createChild(null, null, input));

            then(childClient).shouldHaveNoInteractions();
            then(childParentClient).shouldHaveNoInteractions();
        }

        @Test
        @DisplayName("calls client, and adds Parent role")
        void givenFeignException_Conflict_returnsDuplicateChildException() {
            // Arrange
            //    Look up Parent ID
            given(credentialsClient.findByEmail(any(), any())).willReturn(Optional.of(new User()));

            final Child input = new Child();
            final String expectedEmail = "expected_email";
            input.setEmail(expectedEmail);
            final FeignException feignException = mock(FeignException.Conflict.class);
            doThrow(feignException).when(childClient).createChild(any(), any());

            // Act / Assert
            final DuplicateChildException duplicateChildException =
                    assertThrows(DuplicateChildException.class, () -> service.createChild(null, null, input));

            assertThat(duplicateChildException.getMessage(), equalTo("Child already exists: " + expectedEmail));
        }
    }

    @Nested
    @DisplayName("getAllChildren")
    @Tag("Get_All_Children")
    class GetAllChildren {

        @Test
        @DisplayName("happy path")
        void happyPath() {
            // Arrange
            //    Look up parent's credentials ID
            final String expectedParentEmail = "expected_parent_email";
            final User parent = new User();
            final long parentId = 123L;
            parent.setId(parentId);
            final String expectedToken = "expected_token";
            given(credentialsClient.findByEmail(expectedToken, expectedParentEmail)).willReturn(Optional.of(parent));

            //    Get list of Children IDs for Parent
            final ChildParent childParent = new ChildParent();
            final long childId = 456L;
            childParent.setChildId(childId);
            given(childParentClient.findByParentId(expectedToken, parentId))
                    .willReturn(CollectionModel.of(singletonList(childParent)));

            //    Get Children by IDs
            final Child expectedChild = new Child();
            expectedChild.setFirstName("1234_first_name");
            expectedChild.setEmail("some_email");
            final CollectionModel<Child> expectedResponse = CollectionModel.of(singletonList(expectedChild));
            given(childClient.findAllByIds(expectedToken, singletonList(childId))).willReturn(expectedResponse);

            // Act
            final Collection<Child> response = service.getAllChildren(expectedToken, expectedParentEmail);

            // Assert
            assertThat("Service returned a null response", response, notNullValue());
            assertThat("Service returned empty response", response, hasSize(greaterThan(0)));
            assertThat("Service returned response with more than one entity", response, hasSize(1));
            final ArrayList<Child> responseAsList = new ArrayList<>(response);
            assertThat("Service returned wrong entity", responseAsList.get(0), equalTo(expectedChild));
        }

        @Test
        @DisplayName("happy path, multiple children")
        void multiple_children() {
            // Arrange
            //    Look up parent's credentials ID
            final String expectedParentEmail = "expected_parent_email";
            final User parent = new User();
            final long parentId = 123L;
            parent.setId(parentId);
            final String expectedToken = "expected_token";
            given(credentialsClient.findByEmail(expectedToken, expectedParentEmail)).willReturn(Optional.of(parent));

            //    Get list of Children IDs for Parent
            final ChildParent childParentA = new ChildParent();
            final long childIdA = 456L;
            childParentA.setChildId(childIdA);
            final ChildParent childParentB = new ChildParent();
            final long childIdB = 789L;
            childParentB.setChildId(childIdB);
            given(childParentClient.findByParentId(expectedToken, parentId))
                    .willReturn(CollectionModel.of(asList(childParentA, childParentB)));

            //    Get Children by IDs
            final Child expectedChildA = new Child();
            expectedChildA.setFirstName("1234_first_name_A");
            expectedChildA.setEmail("some_email_A");
            final Child expectedChildB = new Child();
            expectedChildB.setFirstName("1234_first_name_B");
            expectedChildB.setEmail("some_email_B");
            final CollectionModel<Child> expectedResponse = CollectionModel.of(asList(expectedChildA, expectedChildB));
            given(childClient.findAllByIds(expectedToken, asList(childIdA, childIdB))).willReturn(expectedResponse);

            // Act
            final Collection<Child> response = service.getAllChildren(expectedToken, expectedParentEmail);

            // Assert
            assertThat("Service returned a null response", response, notNullValue());
            assertThat("Service returned empty response", response, hasSize(greaterThan(0)));
            assertThat("Service returned response with only one entity", response, hasSize(greaterThan(1)));
            assertThat("Service returned response with more than two entities", response, hasSize(2));
            final ArrayList<Child> responseAsList = new ArrayList<>(response);
            assertThat("Service returned wrong first entity", responseAsList.get(0), equalTo(expectedChildA));
            assertThat("Service returned wrong first entity", responseAsList.get(1), equalTo(expectedChildB));
        }

        @Test
        @DisplayName("parent not found")
        void parentNotFound() {
            // Arrange
            //    Look up parent's credentials ID, but find nothing
            final String expectedParentEmail = "expected_parent_email";
            final User parent = new User();
            final long parentId = 123L;
            parent.setId(parentId);
            final String expectedToken = "expected_token";
            given(credentialsClient.findByEmail(expectedToken, expectedParentEmail)).willReturn(Optional.empty());

            // Act / Assert
            assertThrows(EntityNotFoundException.class, () -> service.getAllChildren(expectedToken, expectedParentEmail));
        }

        @Test
        @DisplayName("parent has no children")
        void parentHasNoChildren_shouldReturnEmptyList() {
            // Arrange
            //    Look up parent's credentials ID
            final String expectedParentEmail = "expected_parent_email";
            final User parent = new User();
            final long parentId = 123L;
            parent.setId(parentId);
            final String expectedToken = "expected_token";
            given(credentialsClient.findByEmail(expectedToken, expectedParentEmail)).willReturn(Optional.of(parent));

            //    Get list of Children IDs for Parent
            given(childParentClient.findByParentId(expectedToken, parentId))
                    .willReturn(CollectionModel.of(emptyList()));

            // Act
            final Collection<Child> response = service.getAllChildren(expectedToken, expectedParentEmail);

            // Assert
            then(childClient).shouldHaveNoInteractions();
            assertThat("Service returned a null response", response, notNullValue());
            assertThat("Service returned non-empty response", response, hasSize(0));
        }
    }

    @Nested
    @DisplayName("getChild")
    @Tag("Get_Child")
    class GetChild {

        @Test
        @DisplayName("happy path")
        void happyPath() {
            // Arrange
            final Child expectedChild = new Child();
            expectedChild.setFirstName("1234_first_name");
            expectedChild.setEmail("some_email");
            final long expectedChildId = 1234L;
            final String expectedToken = "expected_token";
            given(childClient.getChild(expectedToken, expectedChildId)).willReturn(Optional.of(expectedChild));

            // Act
            final Child response = service.getChild(expectedToken, expectedChildId);

            // Assert
            assertThat("Service returned a null response", response, is(notNullValue()));
            assertThat("Service returned entity that doesn't match", response, is(equalTo(expectedChild)));
        }

        @Test
        @DisplayName("Child not found, should return empty Optional")
        void childNotFound_shouldThrowEntityNotFoundException() {
            // Arrange
            final String expectedToken = "expected_token";
            final long childId = 4567L;
            given(childClient.getChild(expectedToken, childId)).willReturn(Optional.empty());

            // Act / Assert
            assertThrows(EntityNotFoundException.class, () -> service.getChild(expectedToken, childId));
        }
    }

    @Nested
    @DisplayName("assignChore")
    @Tag("Assign_Chore")
    class AssignChore {

        @Test
        @DisplayName("happy path")
        void happyPath() {
            // Arrange
            final String expectedToken = "expected_token";
            final long childId = 487428L;
            final long choreId = 587383L;
            final Chore chore = new Chore();
            chore.setId(choreId);
            given(childClient.getChild(expectedToken, childId)).willReturn(Optional.of(new Child()));

            // Act
            service.assignChore(expectedToken, childId, chore);

            // Assert
            final ChoreChild expectedChoreChild = new ChoreChild(childId, choreId);
            then(choreChildClient).should().createChoreChildLink(expectedToken, expectedChoreChild);
        }

        @Test
        @DisplayName("given Child does not exist, should return 'entity not found'")
        void givenChildDoesNotExist_shouldReturnEntityNotFound() {
            // Arrange
            //    Look up Child
            final long childId = 37374L;
            final String token = "token_4848934";
            given(childClient.getChild(token, childId)).willReturn(Optional.empty());

            // Act / Assert
            assertThrows(EntityNotFoundException.class, () -> service.assignChore(token, childId, new Chore()));

            then(choreChildClient).shouldHaveNoInteractions();
        }
    }

    @Nested
    @DisplayName("getChildChores")
    @Tag("Get_Child_Chores")
    class GetChildChores {

        @Test
        @DisplayName("happy path")
        void happyPath() {
            // Arrange
            //    Look up Child
            final String expectedToken = "expected_token";
            final long childId = 329847L;
            given(childClient.getChild(expectedToken, childId)).willReturn(Optional.of(new Child()));

            //    Get list of Chore IDs for Child
            final long choreId = 49048234L;
            final ChoreChild choreChild = new ChoreChild(childId, choreId);
            given(choreChildClient.findByChildId(expectedToken, childId))
                    .willReturn(CollectionModel.of(singletonList(choreChild)));

            //    Get Chores by IDs
            final Chore expectedChore = new Chore();
            expectedChore.setName("1234_name");
            final CollectionModel<Chore> expectedResponse = CollectionModel.of(singletonList(expectedChore));
            given(choreClient.findAllByIds(expectedToken, singletonList(choreId))).willReturn(expectedResponse);

            // Act
            final Collection<Chore> response = service.getChildChores(expectedToken, childId);

            // Assert
            assertThat("Service returned a null response", response, notNullValue());
            assertThat("Service returned empty response", response, hasSize(greaterThan(0)));
            assertThat("Service returned response with more than one entity", response, hasSize(1));
            final ArrayList<Chore> responseAsList = new ArrayList<>(response);
            assertThat("Service returned wrong entity", responseAsList.get(0), equalTo(expectedChore));
        }

        @Test
        @DisplayName("happy path, multiple Chores")
        void multiple_chores() {
            // Arrange
            //    Look up Child
            final String expectedToken = "expected_token";
            final long expectedChildId = 123L;
            given(childClient.getChild(expectedToken, expectedChildId)).willReturn(Optional.of(new Child()));

            //    Get list of Chore IDs for Child
            final long choreIdA = 456L;
            final ChoreChild choreChildA = new ChoreChild(expectedChildId, choreIdA);
            final long choreIdB = 789L;
            final ChoreChild choreChildB = new ChoreChild(expectedChildId, choreIdB);
            given(choreChildClient.findByChildId(expectedToken, expectedChildId))
                    .willReturn(CollectionModel.of(asList(choreChildA, choreChildB)));

            //    Get Chores by IDs
            final Chore expectedChoreA = new Chore();
            expectedChoreA.setName("1234_name_A");
            final Chore expectedChoreB = new Chore();
            expectedChoreB.setName("1234_name_B");
            final CollectionModel<Chore> expectedResponse = CollectionModel.of(asList(expectedChoreA, expectedChoreB));
            given(choreClient.findAllByIds(expectedToken, asList(choreIdA, choreIdB))).willReturn(expectedResponse);

            // Act
            final Collection<Chore> response = service.getChildChores(expectedToken, expectedChildId);

            // Assert
            assertThat("Service returned a null response", response, notNullValue());
            assertThat("Service returned empty response", response, hasSize(greaterThan(0)));
            assertThat("Service returned response with only one entity", response, hasSize(greaterThan(1)));
            assertThat("Service returned response with more than two entities", response, hasSize(2));
            final ArrayList<Chore> responseAsList = new ArrayList<>(response);
            assertThat("Service returned wrong first entity", responseAsList.get(0), equalTo(expectedChoreA));
            assertThat("Service returned wrong first entity", responseAsList.get(1), equalTo(expectedChoreB));
        }

        @Test
        @DisplayName("child not found")
        void childNotFound() {
            // Arrange
            //    Look up Child, but find nothing
            final long expectedChildId = 123L;
            final String expectedToken = "expected_token";
            given(childClient.getChild(expectedToken, expectedChildId)).willReturn(Optional.empty());

            // Act / Assert
            assertThrows(EntityNotFoundException.class, () -> service.getChildChores(expectedToken, expectedChildId));
        }

        @Test
        @DisplayName("Child has no Chores")
        void childHasNoChores_shouldReturnEmptyList() {
            // Arrange
            //    Look up Child
            final Child child = new Child();
            final long childId = 123L;
            child.setId(childId);
            final String expectedToken = "expected_token";
            given(childClient.getChild(expectedToken, childId)).willReturn(Optional.of(child));

            //    Get list of Chore IDs for Child
            given(choreChildClient.findByChildId(expectedToken, childId))
                    .willReturn(CollectionModel.of(emptyList()));

            // Act
            final Collection<Chore> response = service.getChildChores(expectedToken, childId);

            // Assert
            then(choreClient).shouldHaveNoInteractions();
            assertThat("Service returned a null response", response, notNullValue());
            assertThat("Service returned non-empty response", response, hasSize(0));
        }
    }

}