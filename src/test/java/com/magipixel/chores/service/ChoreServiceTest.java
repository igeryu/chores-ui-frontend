package com.magipixel.chores.service;

import com.magipixel.chores.MockitoStrictStubbingTest;
import com.magipixel.chores.client.ChoreClient;
import com.magipixel.chores.client.ChoreParentClient;
import com.magipixel.chores.client.CredentialsClient;
import com.magipixel.chores.exception.DuplicateChoreException;
import com.magipixel.chores.exception.EntityNotFoundException;
import com.magipixel.chores.model.Chore;
import com.magipixel.chores.model.ChoreParent;
import com.magipixel.chores.model.User;
import feign.FeignException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.hateoas.CollectionModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;

@Tag("unit")
class ChoreServiceTest extends MockitoStrictStubbingTest {
    @Mock
    private CredentialsClient credentialsClient;

    @Mock
    private ChoreClient choreClient;

    @Mock
    private ChoreParentClient choreParentClient;

    @InjectMocks
    private ChoreService service;

    @Nested
    @DisplayName("createChore")
    class CreateChoreTest {

        @Test
        @DisplayName("createChore - happy path")
        @Tag("Create_Chore")
        void createChore() throws DuplicateChoreException {
            // Arrange
            //    Look up Parent ID
            final String expectedToken = "expected_token";
            final String parentEmail = "parent_email";
            final User parent = new User();
            final long expectedParentId = 456L;
            parent.setId(expectedParentId);
            given(credentialsClient.findByEmail(expectedToken, parentEmail)).willReturn(Optional.of(parent));

            //    Create new Chore entity
            final Chore choreToCreate = new Chore();
            choreToCreate.setName("chore_name]");
            final Chore createdChore = new Chore();
            final long expectedChoreId = 123L;
            createdChore.setId(expectedChoreId);
            given(choreClient.createChore(expectedToken, choreToCreate)).willReturn(createdChore);

            // Act
            service.createChore(expectedToken, parentEmail, choreToCreate);

            // Assert
            //    Associate new Chore to Parent
            final ChoreParent expectedChore = new ChoreParent();
            expectedChore.setParentId(expectedParentId);
            expectedChore.setChoreId(expectedChoreId);
            then(choreParentClient).should().createChoreParentLink(expectedToken, expectedChore);
        }

        @Test
        @DisplayName("createChore - calls client, and handles duplicate error")
        @Tag("Create_Chore")
        void createChore_givenFeignException_Conflict_returnsDuplicateChoreException() {
            // Arrange
            //    Look up Parent ID
            given(credentialsClient.findByEmail(any(), any())).willReturn(Optional.of(new User()));

            final Chore input = new Chore();
            final String expectedName = "expected_name";
            input.setName(expectedName);
            final FeignException feignException = mock(FeignException.Conflict.class);
            doThrow(feignException).when(choreClient).createChore(any(), any());

            // Act / Assert
            final DuplicateChoreException duplicateChoreException =
                    assertThrows(DuplicateChoreException.class, () -> service.createChore(null, null, input));

            assertThat(duplicateChoreException.getMessage(), equalTo("Chore already exists: " + expectedName));
        }

        @Test
        @DisplayName("createChore - parent not found")
        @Tag("Create_Chore")
        void createChore_parentNotFound() {
            // Arrange
            //    Look up Parent ID
            final String expectedToken = "expected_token";
            final String parentEmail = "parent_email";
            given(credentialsClient.findByEmail(expectedToken, parentEmail)).willReturn(Optional.empty());

            //    Create new Chore entity
            final Chore choreToCreate = new Chore();
            choreToCreate.setName("chore_name]");

            // Act / Assert
            assertThrows(EntityNotFoundException.class, () ->
                    service.createChore(expectedToken, parentEmail, choreToCreate));

            then(choreClient).shouldHaveNoInteractions();
            then(choreParentClient).shouldHaveNoInteractions();
        }
    }

    @Nested
    @DisplayName("getAllChores")
    class GetAllChoresTest {

        @Test
        @DisplayName("happy path: B")
        void happyPath_A() {
            // Arrange
            //    Look up parent's credentials ID
            final String expectedParentEmail = "expected_parent_email";
            final User parent = new User();
            final long parentId = 123L;
            parent.setId(parentId);
            final String expectedToken = "expected_token";
            given(credentialsClient.findByEmail(expectedToken, expectedParentEmail)).willReturn(Optional.of(parent));

            //    Get list of Chores IDs for Parent
            final ChoreParent choreParent = new ChoreParent();
            final long choreId = 456L;
            choreParent.setChoreId(choreId);
            given(choreParentClient.findByParentId(expectedToken, parentId))
                    .willReturn(new CollectionModel<>(singletonList(choreParent)));

            //    Get Chores by IDs
            final Chore expectedChore = new Chore();
            expectedChore.setName("chore_name");
            final CollectionModel<Chore> expectedResponse = new CollectionModel<>(singletonList(expectedChore));
            given(choreClient.findAllByIds(expectedToken, singletonList(choreId))).willReturn(expectedResponse);

            // Act
            final Collection<Chore> response = service.getAllChores(expectedToken, expectedParentEmail);

            // Assert
            assertThat("Service returned a null response", response, notNullValue());
            assertThat("Service returned empty response", response, hasSize(greaterThan(0)));
            assertThat("Service returned response with more than one entity", response, hasSize(1));
            final ArrayList responseAsList = new ArrayList<>(response);
            assertThat("Service returned wrong entity", responseAsList.get(0), equalTo(expectedChore));
        }

        @Test
        @DisplayName("happy path: A")
        void happyPath_B() {
            // Arrange
            //    Look up parent's credentials ID
            final String expectedParentEmail = "expected_parent_email";
            final User parent = new User();
            final long parentId = 789L;
            parent.setId(parentId);
            final String expectedToken = "expected_token";
            given(credentialsClient.findByEmail(expectedToken, expectedParentEmail)).willReturn(Optional.of(parent));

            //    Get list of Chores IDs for Parent
            final ChoreParent choreParent = new ChoreParent();
            final long choreId = 1234L;
            choreParent.setChoreId(choreId);
            given(choreParentClient.findByParentId(expectedToken, parentId))
                    .willReturn(new CollectionModel<>(singletonList(choreParent)));

            //    Get Chores by IDs
            final Chore expectedChore = new Chore();
            expectedChore.setName("chore_name");
            final CollectionModel<Chore> expectedResponse = new CollectionModel<>(singletonList(expectedChore));
            given(choreClient.findAllByIds(expectedToken, singletonList(choreId))).willReturn(expectedResponse);

            // Act
            final Collection<Chore> response = service.getAllChores(expectedToken, expectedParentEmail);

            // Assert
            assertThat("Service returned a null response", response, notNullValue());
            assertThat("Service returned empty response", response, hasSize(greaterThan(0)));
            assertThat("Service returned response with more than one entity", response, hasSize(1));
            final ArrayList responseAsList = new ArrayList<>(response);
            assertThat("Service returned wrong entity", responseAsList.get(0), equalTo(expectedChore));
        }

        @Test
        @DisplayName("happy path, multiple chores")
        void multiple_chores() {
            // Arrange
            //    Look up parent's credentials ID
            final String expectedParentEmail = "expected_parent_email";
            final User parent = new User();
            final long parentId = 123L;
            parent.setId(parentId);
            final String expectedToken = "expected_token";
            given(credentialsClient.findByEmail(expectedToken, expectedParentEmail)).willReturn(Optional.of(parent));

            //    Get list of Chores IDs for Parent
            final ChoreParent choreParentA = new ChoreParent();
            final long choreIdA = 456L;
            choreParentA.setChoreId(choreIdA);
            final ChoreParent choreParentB = new ChoreParent();
            final long choreIdB = 789L;
            choreParentB.setChoreId(choreIdB);
            given(choreParentClient.findByParentId(expectedToken, parentId))
                    .willReturn(new CollectionModel<>(asList(choreParentA, choreParentB)));

            //    Get Chores by IDs
            final Chore expectedChoreA = new Chore();
            expectedChoreA.setName("chore_name_A");
            final Chore expectedChoreB = new Chore();
            expectedChoreB.setName("chore_name_B");
            final CollectionModel<Chore> expectedResponse = new CollectionModel<>(asList(expectedChoreA, expectedChoreB));
            given(choreClient.findAllByIds(expectedToken, asList(choreIdA, choreIdB))).willReturn(expectedResponse);

            // Act
            final Collection<Chore> response = service.getAllChores(expectedToken, expectedParentEmail);

            // Assert
            assertThat("Service returned a null response", response, notNullValue());
            assertThat("Service returned empty response", response, hasSize(greaterThan(0)));
            assertThat("Service returned response with only one entity", response, hasSize(greaterThan(1)));
            assertThat("Service returned response with more than two entities", response, hasSize(2));
            final ArrayList responseAsList = new ArrayList<>(response);
            assertThat("Service returned wrong first entity", responseAsList.get(0), equalTo(expectedChoreA));
            assertThat("Service returned wrong first entity", responseAsList.get(1), equalTo(expectedChoreB));
        }

        @Test
        @DisplayName("Parent not found")
        void parentNotFound() {
            // Arrange
            //    Look up parent's credentials ID, but find nothing
            final String expectedParentEmail = "expected_parent_email";
            final User parent = new User();
            final long parentId = 123L;
            parent.setId(parentId);
            final String expectedToken = "expected_token";
            given(credentialsClient.findByEmail(expectedToken, expectedParentEmail)).willReturn(Optional.empty());

            // Act / Assert
            assertThrows(EntityNotFoundException.class, () -> service.getAllChores(expectedToken, expectedParentEmail));
        }

        @Test
        @DisplayName("parent has no chores")
        void parentHasNoChores_shouldReturnEmptyList() {
            // Arrange
            //    Look up parent's credentials ID
            final String expectedParentEmail = "expected_parent_email";
            final User parent = new User();
            final long parentId = 123L;
            parent.setId(parentId);
            final String expectedToken = "expected_token";
            given(credentialsClient.findByEmail(expectedToken, expectedParentEmail)).willReturn(Optional.of(parent));

            //    Get list of Chores IDs for Parent
            given(choreParentClient.findByParentId(expectedToken, parentId))
                    .willReturn(new CollectionModel<>(emptyList()));

            // Act
            final Collection<Chore> response = service.getAllChores(expectedToken, expectedParentEmail);

            // Assert
            then(choreClient).should(never()).findAllByIds(any(), any());
            assertThat("Service returned a null response", response, notNullValue());
            assertThat("Service returned non-empty response", response, hasSize(0));
        }

    }

}