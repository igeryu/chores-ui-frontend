package com.magipixel.chores.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.magipixel.chores.MockitoStrictStubbingTest;
import com.magipixel.chores.exception.DuplicateChildException;
import com.magipixel.chores.exception.EntityNotFoundException;
import com.magipixel.chores.model.Child;
import com.magipixel.chores.model.Chore;
import com.magipixel.chores.service.ChildService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;

import java.security.Principal;
import java.util.Collection;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsSame.sameInstance;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

@Tag("unit")
class ChildControllerTest extends MockitoStrictStubbingTest {
    @Mock
    private ChildService service;

    @Mock
    private ObjectMapper mapper;

    @InjectMocks
    private ChildController controller;

    @Nested
    @DisplayName("Create Child")
    @Tag("Create_Child")
    class CreateChild {

        @Test
        @DisplayName("Create Child happy path")
        void happyPath() throws DuplicateChildException {
            // Arrange
            final Child child = new Child();
            child.setFirstName("1234_first_name]");
            child.setEmail("some_email");
            final String expectedToken = "expected_token";
            final Principal principal = mock(Principal.class);
            final String expectedParentEmail = "expected_parent_email";
            given(principal.getName()).willReturn(expectedParentEmail);

            // Act
            controller.createChild(expectedToken, principal, child);

            // Assert
            then(service).should().createChild(expectedToken, expectedParentEmail, child);
        }

        @Test
        @DisplayName("Create Child no Authorization header, should throw AuthenticationCredentialsNotFoundException")
        void noAuthorizationHeader_shouldThrowAuthenticationCredentialsNotFoundException() {
            // Arrange
            final Child child = new Child();
            child.setFirstName("1234_first_name]");
            child.setEmail("some_email");

            // Act / Assert
            final AuthenticationCredentialsNotFoundException thrown
                    = assertThrows(AuthenticationCredentialsNotFoundException.class,
                    () -> controller.createChild(null, mock(Principal.class), child));

            assertThat("Exception thrown incorrect message", thrown.getMessage(),
                    equalTo("Not Authorized: Request missing 'Authorization' header"));
        }

    }

    @Nested
    @DisplayName("Get All Children")
    @Tag("Get_All_Children")
    class GetAllChildren {

        @Test
        @DisplayName("getAllChildren happy path")
        void happyPath() throws JsonProcessingException {
            // Arrange
            final Child child = new Child();
            child.setFirstName("1234_first_name]");
            child.setEmail("some_email");
            final List<Child> expectedResponse = singletonList(child);
            final String expectedToken = "expected_token";
            final Principal principal = mock(Principal.class);
            final String expectedEmail = "expected_email";
            given(principal.getName()).willReturn(expectedEmail);
            given(service.getAllChildren(expectedToken, expectedEmail)).willReturn(expectedResponse);

            // Act
            final Collection<Child> response = controller.getAllChildren(expectedToken, principal);

            // Assert
            then(service).should().getAllChildren(any(), any());
            then(service).should().getAllChildren(anyString(), anyString());
            then(service).should().getAllChildren(expectedToken, expectedEmail);
            assertThat(response, sameInstance(expectedResponse));
        }

        @Test
        @DisplayName("getAllChildren no Authorization header, should throw AuthenticationCredentialsNotFoundException")
        void noAuthorizationHeader_shouldThrowAuthenticationCredentialsNotFoundException() {
            // Act / Assert
            final AuthenticationCredentialsNotFoundException thrown
                    = assertThrows(AuthenticationCredentialsNotFoundException.class,
                    () -> controller.getAllChildren(null, null));

            assertThat("Exception thrown incorrect message", thrown.getMessage(),
                    equalTo("Not Authorized: Request missing 'Authorization' header"));
        }

        @Test
        @DisplayName("Kludge: getAllChildren no Principal, should use null for email")
        void noPrincipal_shouldUseNullForEmail() throws JsonProcessingException {
            // Arrange
            final Child child = new Child();
            child.setFirstName("1234_first_name]");
            child.setEmail("some_email");
            final List<Child> expectedResponse = singletonList(child);
            final String expectedToken = "expected_token";
            given(service.getAllChildren(expectedToken, null)).willReturn(expectedResponse);

            // Act
            final Collection<Child> response = controller.getAllChildren(expectedToken, null);

            // Assert
            then(service).should().getAllChildren(any(), eq(null));
            then(service).should().getAllChildren(anyString(), eq(null));
            then(service).should().getAllChildren(expectedToken, null);
            assertThat(response, sameInstance(expectedResponse));
        }

        @Test
        @DisplayName("getAllChildren throws EntityNotFoundException")
        void throwsEntityNotFoundException_whenServiceThrows() {
            // Arrange
            final Child child = new Child();
            child.setFirstName("1234_first_name]");
            child.setEmail("some_email");
            final String expectedToken = "expected_token";
            final Principal principal = mock(Principal.class);
            final String expectedEmail = "expected_email";
            given(principal.getName()).willReturn(expectedEmail);
            given(service.getAllChildren(any(), any())).willThrow(EntityNotFoundException.class);

            // Act / Assert
            assertThrows(EntityNotFoundException.class, () -> controller.getAllChildren(expectedToken, principal));
        }

    }

    @Nested
    @DisplayName("Get Child")
    @Tag("Get_Child")
    class GetChild {

        @Test
        @DisplayName("getChild happy path")
        void happyPath() {
            // Arrange
            final Child expectedResponse = new Child();
            expectedResponse.setFirstName("1234_first_name]");
            expectedResponse.setEmail("some_email");
            final String expectedToken = "expected_token";
            final long expectedId = 789L;
            given(service.getChild(expectedToken, expectedId)).willReturn(expectedResponse);

            // Act
            final Child response = controller.getChild(expectedToken, expectedId);

            // Assert
            then(service).should().getChild(any(), anyLong());
            then(service).should().getChild(anyString(), anyLong());
            then(service).should().getChild(expectedToken, expectedId);
            assertThat(response, is(sameInstance(expectedResponse)));
        }

        @Test
        @DisplayName("no Authorization header, should throw AuthenticationCredentialsNotFoundException")
        void noAuthorizationHeader_shouldThrowAuthenticationCredentialsNotFoundException() {
            // Act / Assert
            final AuthenticationCredentialsNotFoundException thrown
                    = assertThrows(AuthenticationCredentialsNotFoundException.class,
                    () -> controller.getChild(null, -1L));

            assertThat("Exception thrown incorrect message", thrown.getMessage(),
                    equalTo("Not Authorized: Request missing 'Authorization' header"));
        }

        @Test
        @DisplayName("throws EntityNotFoundException")
        void throwsEntityNotFoundException_whenServiceThrows() {
            // Arrange
            final Child child = new Child();
            child.setFirstName("1234_first_name]");
            child.setEmail("some_email");
            final String expectedToken = "expected_token";
            given(service.getChild(any(), anyLong())).willThrow(EntityNotFoundException.class);

            // Act / Assert
            assertThrows(EntityNotFoundException.class, () -> controller.getChild(expectedToken, 678L));
        }

    }

    @Nested
    @DisplayName("Assign Chore")
    @Tag("Assign_Chore")
    class AssignChore {

        @Test
        @DisplayName("Assign Chore happy path")
        void happyPath() {
            // Arrange
            final String expectedToken = "expected_token";
            final long expectedChildId = 348584L;
            final Chore expectedChore = new Chore();

            // Act
            controller.assignChore(expectedToken, expectedChildId, expectedChore);

            // Assert
            then(service).should().assignChore(expectedToken, expectedChildId, expectedChore);
        }

        @Test
        @DisplayName("no Authorization header, should throw AuthenticationCredentialsNotFoundException")
        void noAuthorizationHeader_shouldThrowAuthenticationCredentialsNotFoundException() {
            // Act / Assert
            final AuthenticationCredentialsNotFoundException thrown
                    = assertThrows(AuthenticationCredentialsNotFoundException.class,
                    () -> controller.assignChore(null, 5834597L, new Chore()));

            assertThat("Exception thrown incorrect message", thrown.getMessage(),
                    equalTo("Not Authorized: Request missing 'Authorization' header"));
        }

    }

    @Nested
    @DisplayName("Get Child's Chores")
    @Tag("Get_Child_Chores")
    class GetChildChores {

        @Test
        @DisplayName("happy path")
        void happyPath() {
            // Arrange
            final Chore chore = new Chore();
            chore.setName("1234_name");
            final String expectedToken = "expected_token";
            final long expectedChildId = 789L;
            final List<Chore> expectedResponse = singletonList(chore);
            given(service.getChildChores(expectedToken, expectedChildId)).willReturn(expectedResponse);

            // Act
            final Collection<Chore> response = controller.getChildChores(expectedToken, expectedChildId);

            // Assert
            then(service).should().getChildChores(any(), anyLong());
            then(service).should().getChildChores(anyString(), anyLong());
            then(service).should().getChildChores(expectedToken, expectedChildId);
            assertThat(response, is(sameInstance(expectedResponse)));
        }

        @Test
        @DisplayName("no Authorization header, should throw AuthenticationCredentialsNotFoundException")
        void noAuthorizationHeader_shouldThrowAuthenticationCredentialsNotFoundException() {
            // Act / Assert
            final AuthenticationCredentialsNotFoundException thrown
                    = assertThrows(AuthenticationCredentialsNotFoundException.class,
                    () -> controller.getChildChores(null, -1L));

            assertThat("Exception thrown incorrect message", thrown.getMessage(),
                    equalTo("Not Authorized: Request missing 'Authorization' header"));
        }

        @Test
        @DisplayName("throws EntityNotFoundException")
        void throwsEntityNotFoundException_whenServiceThrows() {
            // Arrange
            final String expectedToken = "expected_token";
            given(service.getChildChores(any(), anyLong())).willThrow(EntityNotFoundException.class);

            // Act / Assert
            assertThrows(EntityNotFoundException.class, () -> controller.getChildChores(expectedToken, 678L));
        }

    }

}