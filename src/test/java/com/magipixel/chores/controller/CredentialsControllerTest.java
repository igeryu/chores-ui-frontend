package com.magipixel.chores.controller;

import com.magipixel.chores.MockitoStrictStubbingTest;
import com.magipixel.chores.exception.DuplicateAccountException;
import com.magipixel.chores.model.User;
import com.magipixel.chores.service.CredentialsService;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.BDDMockito.then;

@Tag("unit")
class CredentialsControllerTest extends MockitoStrictStubbingTest {

    @Mock
    private CredentialsService service;

    @InjectMocks
    private CredentialsController controller;

    @Test
    void createCredentials() throws DuplicateAccountException {
        // Arrange
        final User user = new User();
        user.setPassword("1234_password]");
        user.setUsername("some_username");

        // Act
        controller.createCredentials(user);

        // Assert
        then(service).should().createCredentials(user);
    }

}