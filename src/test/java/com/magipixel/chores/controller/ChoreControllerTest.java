package com.magipixel.chores.controller;

import com.magipixel.chores.MockitoStrictStubbingTest;
import com.magipixel.chores.exception.DuplicateChoreException;
import com.magipixel.chores.exception.EntityNotFoundException;
import com.magipixel.chores.model.Chore;
import com.magipixel.chores.service.ChoreService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;

import java.security.Principal;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsSame.sameInstance;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

@Tag("unit")
class ChoreControllerTest extends MockitoStrictStubbingTest {
    @Mock
    private ChoreService service;

    @InjectMocks
    private ChoreController controller;

    @Test
    @DisplayName("createChore - happy path")
    @Tag("Create_Chore")
    void createChore() throws DuplicateChoreException {
        // Arrange
        final Chore chore = new Chore();
        chore.setName("chore_name]");
        final String expectedToken = "expected_token";
        final Principal principal = mock(Principal.class);
        final String expectedParentEmail = "expected_parent_email";
        given(principal.getName()).willReturn(expectedParentEmail);

        // Act
        controller.createChore(expectedToken, principal, chore);

        // Assert
        then(service).should().createChore(expectedToken, expectedParentEmail, chore);
    }

    @Test
    @DisplayName("createChore - no Authorization header, should throw AuthenticationCredentialsNotFoundException")
    @Tag("Create_Chore")
    void createChore_noAuthorizationHeader_shouldThrowAuthenticationCredentialsNotFoundException() {
        // Arrange
        final Chore chore = new Chore();
        chore.setName("chore_name]");

        // Act / Assert
        final AuthenticationCredentialsNotFoundException thrown
                = assertThrows(AuthenticationCredentialsNotFoundException.class,
                () -> controller.createChore(null, mock(Principal.class), chore));

        assertThat("Exception thrown incorrect message", thrown.getMessage(),
                equalTo("Not Authorized: Request missing 'Authorization' header"));
    }

    @Test
    @DisplayName("getAllChores happy path")
    @Tag("Get_All_Chores")
    void getAllChores() {
        // Arrange
        final Chore chore = new Chore();
        chore.setName("chore_name]");
        final List<Chore> expectedResponse = singletonList(chore);
        final String expectedToken = "expected_token";
        final Principal principal = mock(Principal.class);
        final String expectedEmail = "expected_email";
        given(principal.getName()).willReturn(expectedEmail);
        given(service.getAllChores(expectedToken, expectedEmail)).willReturn(expectedResponse);

        // Act
        final Iterable<Chore> response = controller.getAllChores(expectedToken, principal);

        // Assert
        then(service).should().getAllChores(any(), any());
        then(service).should().getAllChores(anyString(), anyString());
        then(service).should().getAllChores(expectedToken, expectedEmail);
        assertThat(response, sameInstance(expectedResponse));
    }

    @Test
    @DisplayName("getAllChores no Authorization header, should throw AuthenticationCredentialsNotFoundException")
    @Tag("Get_All_Chores")
    void getAllChores_noAuthorizationHeader_shouldThrowAuthenticationCredentialsNotFoundException() {
        // Act / Assert
        final AuthenticationCredentialsNotFoundException thrown
                = assertThrows(AuthenticationCredentialsNotFoundException.class,
                () -> controller.getAllChores(null, null));

        assertThat("Exception thrown incorrect message", thrown.getMessage(),
                equalTo("Not Authorized: Request missing 'Authorization' header"));
    }

    @Test
    @DisplayName("Kludge: getAllChores no Principal, should use null for email")
    @Tag("Get_All_Chores")
    void getAllChores_noPrincipal_shouldUseNullForEmail() {
        // Arrange
        final Chore chore = new Chore();
        chore.setName("chore_name]");
        final List<Chore> expectedResponse = singletonList(chore);
        final String expectedToken = "expected_token";
        given(service.getAllChores(expectedToken, null)).willReturn(expectedResponse);

        // Act
        final Iterable<Chore> response = controller.getAllChores(expectedToken, null);

        // Assert
        then(service).should().getAllChores(any(), eq(null));
        then(service).should().getAllChores(anyString(), eq(null));
        then(service).should().getAllChores(expectedToken, null);
        assertThat(response, sameInstance(expectedResponse));
    }

    @Test
    @DisplayName("getAllChores throws EntityNotFoundException")
    @Tag("Get_All_Chores")
    void getAllChores_throwsEntityNotFoundException_whenServiceThrows() {
        // Arrange
        final Chore chore = new Chore();
        chore.setName("chore_name]");
        final String expectedToken = "expected_token";
        final Principal principal = mock(Principal.class);
        final String expectedEmail = "expected_email";
        given(principal.getName()).willReturn(expectedEmail);
        given(service.getAllChores(any(), any())).willThrow(EntityNotFoundException.class);

        // Act / Assert
        assertThrows(EntityNotFoundException.class, () -> controller.getAllChores(expectedToken, principal));
    }

}