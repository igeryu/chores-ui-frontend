package com.magipixel.chores.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.magipixel.chores.MockitoStrictStubbingTest;
import com.magipixel.chores.exception.DuplicateChildException;
import com.magipixel.chores.exception.EntityNotFoundException;
import com.magipixel.chores.exception.ErrorResponse;
import com.magipixel.chores.model.Child;
import com.magipixel.chores.model.Chore;
import com.magipixel.chores.service.ChildService;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@Tag("integration")
class ChildControllerIT {

    private static final String PATH_CHILDREN = "/children";

    @Nested
    @DisplayName("HTTP Mapping")
    @WebMvcTest(ChildController.class)
    class Mapping extends MockitoStrictStubbingTest {

        @MockBean
        private ChildController controller;

        private MockMvc mockMvc;

        @Autowired
        private ObjectMapper objectMapper;

        @BeforeEach
        void setup() {
            MockitoAnnotations.openMocks(this);
            mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        }

        @Test
        @DisplayName("Given User is Parent, createChild is mapped and succeeds")
        void givenUserIsParent_createChild_isMapped_andSucceeds() throws Exception {
            // Arrange
            final String content =
                    IOUtils.toString(this.getClass().getResourceAsStream("/json/bb-child-crud/New_Child_Request.json"));

            // Act / Assert
            mockMvc.perform(post(PATH_CHILDREN)
                    .header("Authorization", "Bearer can_be_anything")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(content))
                    .andExpect(status()
                            .isCreated());

            final Child expectedChild = objectMapper.readValue(content, Child.class);
            then(controller).should().createChild("Bearer can_be_anything", null, expectedChild);
        }

        @Test
        @DisplayName("Given User is Parent, getAllChildren is mapped and succeeds")
        void givenUserIsParent_getAllChildren_isMapped_andSucceeds() throws Exception {
            // Act / Assert
            mockMvc.perform(get(PATH_CHILDREN)
                    .header("Authorization", "Bearer can_be_anything")
                    .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status()
                            .isOk())
                    .andDo(print());

            then(controller).should().getAllChildren(eq("Bearer can_be_anything"), any());
        }

        @Test
        @DisplayName("Given User is Parent, getChild is mapped and succeeds")
        void givenUserIsParent_getChild_isMapped_andSucceeds() throws Exception {
            // Act / Assert
            final long expectedChildId = 123L;
            mockMvc.perform(get(PATH_CHILDREN + "/" + expectedChildId)
                    .header("Authorization", "Bearer can_be_anything")
                    .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status()
                            .isOk())
                    .andDo(print());

            then(controller).should().getChild(eq("Bearer can_be_anything"), eq(expectedChildId));
        }

        @Test
        @DisplayName("Given User is Parent, assignChore is mapped and succeeds")
        void givenUserIsParent_assignChore_isMapped_andSucceeds() throws Exception {
            // Arrange
            final String content =
                    IOUtils.toString(this.getClass().getResourceAsStream("/json/bb-chore-crud/New_Chore_Request.json"));
            final long expectedChildId = 1234L;

            // Act / Assert
            mockMvc.perform(put(PATH_CHILDREN + "/" + expectedChildId + "/chores")
                    .header("Authorization", "Bearer can_be_anything")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(content))
                    .andExpect(status()
                            .isAccepted());

            final Chore expectedChore = new Chore();
            expectedChore.setName("task_name");
            then(controller).should().assignChore("Bearer can_be_anything", expectedChildId, expectedChore);
        }

        @Test
        @DisplayName("Given User is Parent, assignChore is mapped and succeeds")
        void givenUserIsParent_getChildChores_isMapped_andSucceeds() throws Exception {
            // Arrange
            final String content =
                    IOUtils.toString(this.getClass().getResourceAsStream("/json/bb-chore-crud/New_Chore_Response.json"));
            final long expectedChildId = 1234L;

            // Act / Assert
            mockMvc.perform(get(PATH_CHILDREN + "/" + expectedChildId + "/chores")
                    .header("Authorization", "Bearer can_be_anything")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(content))
                    .andExpect(status()
                            .isOk());

            then(controller).should().getChildChores("Bearer can_be_anything", expectedChildId);
        }
    }

    @Nested
    @WebMvcTest({ChildController.class, ChildService.class})
    @DisplayName("Error Handling")
    class ErrorHandling extends MockitoStrictStubbingTest {
        @Autowired
        private ChildController controller;

        @MockBean
        private ChildService service;

        private MockMvc mockMvc;

        @Autowired
        private ObjectMapper mapper;

        @BeforeEach
        void setup() {
            MockitoAnnotations.openMocks(this);
            mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        }

        @Test
        @DisplayName("409 whenever service throws FeignException.Conflict")
        @WithMockUser(roles = "PARENT")
        void handlesDuplicateChildException() throws Exception {
            // Arrange
            final String content =
                    IOUtils.toString(this.getClass().getResourceAsStream("/json/bb-child-crud/New_Child_Request.json"));
            final String expectedMessage = "expected_message";
            final DuplicateChildException duplicateChildException = new DuplicateChildException(expectedMessage);
            doThrow(duplicateChildException).when(service).createChild(any(), any(), any(Child.class));

            // Act / Assert
            final String responseString = mockMvc.perform(post(PATH_CHILDREN)
                    .header("Authorization", "Bearer can_be_anything")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(content))
                    .andDo(print())
                    .andExpect(status()
                            .isConflict())
                    .andReturn()
                    .getResponse()
                    .getContentAsString();

            final ErrorResponse expectedError = new ErrorResponse(expectedMessage);
            assertThat(mapper.readValue(responseString, ErrorResponse.class), equalTo(expectedError));
        }

        @Test
        @DisplayName("404 whenever service throws EntityNotFoundException")
        @WithMockUser(roles = "PARENT")
        void getAllChildren_handlesEntityNotFoundException() throws Exception {
            // Arrange
            given(service.getAllChildren(any(), any())).willThrow(EntityNotFoundException.class);

            // Act / Assert
            mockMvc.perform(get(PATH_CHILDREN)
                    .header("Authorization", "Bearer can_be_anything")
                    .contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status()
                            .isNotFound());
        }
    }

    @Nested
    @WebMvcTest({ChildController.class, ChildService.class})
    @DisplayName("Entity Serialization")
    class EntitySerialization extends MockitoStrictStubbingTest {
        @Autowired
        private ChildController controller;

        @MockBean
        private ChildService service;

        private MockMvc mockMvc;

        @Autowired
        private ObjectMapper mapper;

        @BeforeEach
        void setup() {
            MockitoAnnotations.openMocks(this);
            mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        }

        @Test
        @DisplayName("Child entity should include ID")
        @WithMockUser(roles = "PARENT")
        void childEntityShouldIncludeId() throws Exception {
            // Arrange
            final Child child = new Child();
            final long expectedChildId = 7890L;
            child.setId(expectedChildId);
            given(service.getAllChildren(any(), any())).willReturn(Collections.singletonList(child));

            // Act / Assert
            final String responseString = mockMvc.perform(get(PATH_CHILDREN)
                    .header("Authorization", "Bearer can_be_anything")
                    .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status()
                            .isOk())
                    .andDo(print())
                    .andReturn()
                    .getResponse()
                    .getContentAsString();

            assertThat(mapper.readValue(responseString,
                    new TypeReference<List<Child>>() {
                    }).get(0).getId(),
                    equalTo(expectedChildId));
        }
    }
}