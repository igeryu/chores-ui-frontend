package com.magipixel.chores.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.magipixel.chores.MockitoStrictStubbingTest;
import com.magipixel.chores.exception.DuplicateAccountException;
import com.magipixel.chores.exception.ErrorResponse;
import com.magipixel.chores.model.User;
import com.magipixel.chores.service.CredentialsService;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@Tag("integration")
class CredentialsControllerIT extends MockitoStrictStubbingTest {

    @Nested
    @DisplayName("HTTP Mapping")
    @WebMvcTest(CredentialsController.class)
    class Mapping {

        @MockBean
        private CredentialsController controller;

        private MockMvc mockMvc;

        @Autowired
        private ObjectMapper objectMapper;

        @BeforeEach
        void setup() {
            MockitoAnnotations.openMocks(this);
            mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        }

        @Test
        @DisplayName("Given User is not authenticated, createCredentials is mapped and succeeds")
        void createCredentials_isMapped() throws Exception {
            // Arrange
            final String content =
                    IOUtils.toString(this.getClass().getResourceAsStream("/json/bb-credentials/New_Credentials_Response.json"));

            // Act / Assert
            mockMvc.perform(post("/credentials/users")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(content))
                    .andExpect(status()
                            .isCreated());

            final User expectedUser = objectMapper.readValue(content, User.class);
            then(controller).should().createCredentials(expectedUser);
        }

        @Test
        @DisplayName("Sanity check")
        void sanityCheck() throws Exception {
            // Arrange
            final String content =
                    IOUtils.toString(this.getClass().getResourceAsStream("/json/bb-credentials/New_Credentials_Response.json"));

            // Act / Assert
            mockMvc.perform(post("/credentials/can_be_anything")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(content))
                    .andExpect(status()
                            .isNotFound());
        }

    }

    @Nested
    @WebMvcTest({CredentialsController.class, CredentialsService.class})
    @DisplayName("Error Handling")
    class ErrorHandling {
        @MockBean
        private CredentialsService service;

        @Autowired
        private MockMvc mockMvc;

        @Autowired
        private ObjectMapper mapper;

        @Test
        @DisplayName("409 whenever service throws FeignException.Conflict")
        void handlesDuplicateAccountException() throws Exception {
            // Arrange
            final String content =
                    IOUtils.toString(this.getClass().getResourceAsStream("/json/bb-credentials/New_Credentials_Response.json"));
            final String expectedMessage = "expected_message";
            final DuplicateAccountException duplicateAccountException = new DuplicateAccountException(expectedMessage);
            doThrow(duplicateAccountException).when(service).createCredentials(any(User.class));

            // Act / Assert
            final String responseString = mockMvc.perform(post("/credentials/users")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(content))
                    .andDo(print())
                    .andExpect(status()
                            .isConflict())
                    .andReturn()
                    .getResponse()
                    .getContentAsString();

            final ErrorResponse expectedError = new ErrorResponse(expectedMessage);
            assertThat(mapper.readValue(responseString, ErrorResponse.class), equalTo(expectedError));
        }
    }


}