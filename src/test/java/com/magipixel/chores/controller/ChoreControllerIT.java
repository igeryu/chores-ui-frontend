package com.magipixel.chores.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.magipixel.chores.MockitoStrictStubbingTest;
import com.magipixel.chores.exception.DuplicateChoreException;
import com.magipixel.chores.exception.EntityNotFoundException;
import com.magipixel.chores.exception.ErrorResponse;
import com.magipixel.chores.model.Child;
import com.magipixel.chores.model.Chore;
import com.magipixel.chores.service.ChoreService;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@Tag("integration")
class ChoreControllerIT {

    private static final String PATH_CHORES = "/chores";

    @Nested
    @DisplayName("HTTP Mapping")
    @WebMvcTest(ChoreController.class)
    class Mapping extends MockitoStrictStubbingTest {

        @MockBean
        private ChoreController controller;

        private MockMvc mockMvc;

        @Autowired
        private ObjectMapper objectMapper;

        @BeforeEach
        void setup() {
            MockitoAnnotations.openMocks(this);
            mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        }

        @Test
        @DisplayName("Given User is Parent, createChore is mapped and succeeds")
        void givenUserIsParent_createChore_isMapped_andSucceeds() throws Exception {
            // Arrange
            final String content = IOUtils
                    .toString(this.getClass().getResourceAsStream("/json/bb-chore-crud/New_Chore_Request.json"));

            // Act / Assert
            mockMvc.perform(post(PATH_CHORES)
                    .header("Authorization", "Bearer can_be_anything")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(content))
                    .andExpect(status()
                            .isCreated());

            final Chore expectedChore = objectMapper.readValue(content, Chore.class);
            then(controller).should().createChore("Bearer can_be_anything", null, expectedChore);
        }

        @Test
        @DisplayName("Given User is Parent, getAllChores is mapped and succeeds")
        void givenUserIsParent_getAllChores_isMapped_andSucceeds() throws Exception {
            // Act / Assert
            mockMvc.perform(get(PATH_CHORES)
                    .header("Authorization", "Bearer can_be_anything")
                    .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status()
                            .isOk())
                    .andDo(print());

            then(controller).should().getAllChores(eq("Bearer can_be_anything"), any());
        }
    }

    @Nested
    @WebMvcTest({ChoreController.class, ChoreService.class})
    @DisplayName("Error Handling")
    class ErrorHandling extends MockitoStrictStubbingTest {
        @Autowired
        private ChoreController controller;

        @MockBean
        private ChoreService service;

        private MockMvc mockMvc;

        @Autowired
        private ObjectMapper mapper;

        @BeforeEach
        void setup() {
            MockitoAnnotations.openMocks(this);
            mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        }

        @Test
        @DisplayName("409 whenever service throws FeignException.Conflict")
        @WithMockUser(roles = "PARENT")
        void handlesDuplicateChoreException() throws Exception {
            // Arrange
            final String content = IOUtils
                    .toString(this.getClass().getResourceAsStream("/json/bb-chore-crud/New_Chore_Request.json"));
            final String expectedMessage = "expected_message";
            final DuplicateChoreException duplicateChoreException = new DuplicateChoreException(expectedMessage);
            doThrow(duplicateChoreException).when(service).createChore(any(), any(), any(Chore.class));

            // Act / Assert
            final String responseString = mockMvc.perform(post(PATH_CHORES)
                    .header("Authorization", "Bearer can_be_anything")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(content))
                    .andDo(print())
                    .andExpect(status()
                            .isConflict())
                    .andReturn()
                    .getResponse()
                    .getContentAsString();

            final ErrorResponse expectedError = new ErrorResponse(expectedMessage);
            assertThat(mapper.readValue(responseString, ErrorResponse.class), equalTo(expectedError));
        }

        @Test
        @DisplayName("createChore handles Parent not found")
        @WithMockUser(roles = "PARENT")
        void createChore_handlesParentNotFoundException() throws Exception {
            // Arrange
            doThrow(EntityNotFoundException.class).when(service).createChore(any(), any(), any());

            // Act / Assert
            final String responseString = mockMvc.perform(post(PATH_CHORES)
                    .header("Authorization", "Bearer can_be_anything")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{}"))
                    .andDo(print())
                    .andExpect(status()
                            .isBadRequest())
                    .andReturn()
                    .getResponse()
                    .getContentAsString();

            final String expectedMessage = "Parent not found";
            final ErrorResponse expectedError = new ErrorResponse(expectedMessage);
            assertThat(mapper.readValue(responseString, ErrorResponse.class), equalTo(expectedError));
        }
    }

    @Nested
    @WebMvcTest({ChoreController.class, ChoreService.class})
    @DisplayName("Entity Serialization")
    class EntitySerialization extends MockitoStrictStubbingTest {
        @Autowired
        private ChoreController controller;

        @MockBean
        private ChoreService service;

        private MockMvc mockMvc;

        @Autowired
        private ObjectMapper mapper;

        @BeforeEach
        void setup() {
            MockitoAnnotations.openMocks(this);
            mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        }

        @Test
        @DisplayName("Chore entity should include ID")
        @WithMockUser(roles = "PARENT")
        void choreEntityShouldIncludeId() throws Exception {
            // Arrange
            final Chore chore = new Chore();
            final long expectedChoreId = 7890L;
            chore.setId(expectedChoreId);
            given(service.getAllChores(any(), any())).willReturn(Collections.singletonList(chore));

            // Act / Assert
            final String responseString = mockMvc.perform(get(PATH_CHORES)
                    .header("Authorization", "Bearer can_be_anything")
                    .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status()
                            .isOk())
                    .andDo(print())
                    .andReturn()
                    .getResponse()
                    .getContentAsString();

            assertThat(mapper.readValue(responseString,
                    new TypeReference<List<Child>>() {
                    }).get(0).getId(),
                    equalTo(expectedChoreId));
        }
    }
}