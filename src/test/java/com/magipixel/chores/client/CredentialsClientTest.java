package com.magipixel.chores.client;

import com.magipixel.chores.model.Role;
import com.magipixel.chores.model.User;
import org.apache.commons.io.IOUtils;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.MediaType;

import java.io.IOException;
import java.util.Optional;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToJson;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.reset;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static com.magipixel.chores.hamcrest.OptionalMatcher.present;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static wiremock.com.google.common.net.HttpHeaders.CONTENT_TYPE;

@SpringBootTest
@AutoConfigureWireMock(port = 0)
@Tag("unit")
class CredentialsClientTest {

    @Value("${clients.credentials.path}")
    private String usersPath;

    @Autowired
    private CredentialsClient client;

    @AfterEach
    void resetWireMock() {
        reset();
    }

    @Test
    void createCredentials() throws IOException {
        // Arrange
        final String newCredentialsResponse = IOUtils
                .toString(this.getClass().getResourceAsStream("/json/bb-credentials/New_Credentials_Response.json"));
        final String userJson = IOUtils
                .toString(this.getClass().getResourceAsStream("/json/bb-credentials/New_Credentials_Request.json"));

        stubFor(post(urlMatching(usersPath))
                .withHeader(CONTENT_TYPE, equalTo(MediaType.APPLICATION_JSON_VALUE))
                .withRequestBody(equalToJson(userJson))
                .willReturn(aResponse()
                        .withBody(newCredentialsResponse)));

        final User user = new User();
        user.setUsername("user");
        user.setPassword("pass");
        user.setRole(Role.PARENT);

        // Act
        client.createCredentials(user);

        // Assert
        verify(postRequestedFor(urlMatching(usersPath)));
    }

    @Test
    void findByEmail() throws IOException {
        // Arrange
        final String usersResponseJson = IOUtils.toString(
                this.getClass().getResourceAsStream("/json/bb-credentials/New_Credentials_Response.json"));
        final String expectedBearerToken = "can_be_anything";

        final String expectedParentEmail = "expected_parent_email";
        stubFor(get(urlPathEqualTo(usersPath + "/search/findByUsername"))
                .withHeader("Authorization", matching(expectedBearerToken))
                .withQueryParam("username", equalTo(expectedParentEmail))
                .willReturn(aResponse()
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                        .withBody(usersResponseJson)));

        // Act
        final Optional<User> userOptional = client.findByEmail(expectedBearerToken, expectedParentEmail);

        // Assert
        assertThat("Did not get a User from client call", userOptional, is(present()));
        final User expectedUser = new User();
        expectedUser.setId(1);
        expectedUser.setUsername("user");
        expectedUser.setPassword("some_hashed_password");
        expectedUser.setRole(Role.PARENT);
        assertThat("Got wrong User", userOptional.get(), IsEqual.equalTo(expectedUser));
    }

}