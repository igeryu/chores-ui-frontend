package com.magipixel.chores.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.magipixel.chores.model.Child;
import org.apache.commons.io.IOUtils;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.hateoas.CollectionModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToJson;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.reset;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static com.magipixel.chores.hamcrest.OptionalMatcher.present;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static wiremock.com.google.common.net.HttpHeaders.ACCEPT;
import static wiremock.com.google.common.net.HttpHeaders.AUTHORIZATION;
import static wiremock.com.google.common.net.HttpHeaders.CONTENT_TYPE;

@SpringBootTest
@AutoConfigureWireMock(port = 0)
@Tag("unit")
class ChildClientTest {

    @Value("${clients.children.path}")
    private String childPath;

    @Autowired
    private ChildClient client;

    @Autowired
    private ObjectMapper objectMapper;

    @AfterEach
    void resetWireMock() {
        reset();
    }

    @Test
    @DisplayName("Create Child")
    void createChild() throws IOException {
        // Arrange
        final String newChildResponse =
                IOUtils.toString(this.getClass().getResourceAsStream("/json/bb-child-crud/Individual_Child_Response.json"));
        final String userJson =
                IOUtils.toString(this.getClass().getResourceAsStream("/json/bb-child-crud/New_Child_Request.json"));
        final String expectedToken = "expected_token";

        stubFor(post(urlMatching(childPath))
                .withHeader(ACCEPT, equalTo(APPLICATION_JSON_VALUE))
                .withHeader(CONTENT_TYPE, equalTo(APPLICATION_JSON_VALUE))
                .withHeader(AUTHORIZATION, equalTo(expectedToken))
                .withRequestBody(equalToJson(userJson))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .withBody(newChildResponse)));

        final Child user = objectMapper.readValue(userJson, Child.class);

        // Act
        final Child childResponse = client.createChild(expectedToken, user);

        // Assert
        verify(postRequestedFor(urlMatching(childPath))
                .withHeader(ACCEPT, equalTo(APPLICATION_JSON_VALUE))
                .withHeader(CONTENT_TYPE, equalTo(APPLICATION_JSON_VALUE))
                .withHeader(AUTHORIZATION, equalTo(expectedToken))
                .withRequestBody(equalToJson(userJson)));

        final Child expectedChild = new Child();
        expectedChild.setId(123L);
        expectedChild.setFirstName("can_be_anything");
        expectedChild.setEmail("some_email");
        assertThat(childResponse, IsEqual.equalTo(expectedChild));
    }

    @Test
    void getAllChildren() throws IOException {
        // Arrange
        final String getAllChildrenResponse = IOUtils
                .toString(this.getClass().getResourceAsStream("/json/bb-child-crud/Get_All_Children_Response.json"));
        final String expectedToken = "expected_token";

        stubFor(get(urlMatching(childPath))
                .withHeader(ACCEPT, equalTo(APPLICATION_JSON_VALUE))
                .withHeader(AUTHORIZATION, equalTo(expectedToken))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .withBody(getAllChildrenResponse)));

        final Child expectedChild = new Child();
        expectedChild.setId(1L);
        expectedChild.setFirstName("John");
        expectedChild.setEmail("somebody@example.com");

        // Act
        final CollectionModel<Child> response = client.getAllChildren(expectedToken);

        // Assert
        verify(getRequestedFor(urlMatching(childPath))
                .withHeader(AUTHORIZATION, equalTo(expectedToken)));

        assertThat("Client returned a null response", response, notNullValue());
        assertThat("Client returned null content", response.getContent(), notNullValue());
        assertThat("Client returned empty content", response.getContent(), hasSize(greaterThan(0)));
        assertThat("Client returned content with only one entity", response.getContent(), hasSize(greaterThan(1)));
        assertThat("Client returned content with more than one entity", response.getContent(), hasSize(2));
        final ArrayList<Child> responseAsList = new ArrayList<>(response.getContent());
        assertThat("Client returned wrong entity", responseAsList.get(0), IsEqual.equalTo(expectedChild));
    }

    @Test
    void findAllByIds() throws IOException {
        // Arrange
        final String getAllChildrenResponse = IOUtils
                .toString(this.getClass().getResourceAsStream("/json/bb-child-crud/Get_All_Children_Response.json"));
        final String expectedToken = "expected_token";

        final List<Long> childIds = asList(1L, 2L);
        final String findByIdsPath = childPath + "/search/findByIdIn";
        stubFor(get(urlPathEqualTo(findByIdsPath))
                .withHeader(ACCEPT, equalTo(APPLICATION_JSON_VALUE))
                .withHeader(AUTHORIZATION, equalTo(expectedToken))
                .withQueryParam("ids", equalTo("1"))
                .withQueryParam("ids", equalTo("2"))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .withBody(getAllChildrenResponse)));

        final Child expectedChild = new Child();
        expectedChild.setId(1L);
        expectedChild.setFirstName("John");
        expectedChild.setEmail("somebody@example.com");

        // Act
        final CollectionModel<Child> response = client.findAllByIds(expectedToken, childIds);

        // Assert
        verify(getRequestedFor(urlPathEqualTo(findByIdsPath))
                .withHeader(ACCEPT, equalTo(APPLICATION_JSON_VALUE))
                .withHeader(AUTHORIZATION, equalTo(expectedToken))
                .withQueryParam("ids", equalTo("1"))
                .withQueryParam("ids", equalTo("2")));

        assertThat("Client returned a null response", response, notNullValue());
        assertThat("Client returned null content", response.getContent(), notNullValue());
        assertThat("Client returned empty content", response.getContent(), hasSize(greaterThan(0)));
        assertThat("Client returned content with only one entity", response.getContent(), hasSize(greaterThan(1)));
        assertThat("Client returned content with more than one entity", response.getContent(), hasSize(2));
        final ArrayList<Child> responseAsList = new ArrayList<>(response.getContent());
        assertThat("Client returned wrong entity", responseAsList.get(0), IsEqual.equalTo(expectedChild));
    }

    @Test
    void getChild() throws IOException {
        // Arrange
        final String individualChildResponse = IOUtils
                .toString(this.getClass().getResourceAsStream("/json/bb-child-crud/Individual_Child_Response.json"));
        final String expectedToken = "expected_token";
        final long expectedChildId = 23423L;
        final String expectedPath = childPath + "/" + expectedChildId;

        stubFor(get(urlMatching(expectedPath))
                .withHeader(ACCEPT, equalTo(APPLICATION_JSON_VALUE))
                .withHeader(AUTHORIZATION, equalTo(expectedToken))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .withBody(individualChildResponse)));

        final Child expectedChild = new Child();
        expectedChild.setId(123L);
        expectedChild.setFirstName("can_be_anything");
        expectedChild.setEmail("some_email");

        // Act
        final Optional<Child> response = client.getChild(expectedToken, expectedChildId);

        // Assert
        verify(getRequestedFor(urlMatching(expectedPath))
                .withHeader(AUTHORIZATION, equalTo(expectedToken)));

        assertThat("Client returned a null response", response, notNullValue());
        assertThat("Client returned empty content", response, is(present()));
        assertThat("Client returned wrong entity", response.get(), IsEqual.equalTo(expectedChild));
    }

}