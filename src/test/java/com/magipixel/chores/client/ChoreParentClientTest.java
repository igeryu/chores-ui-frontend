package com.magipixel.chores.client;

import com.magipixel.chores.model.ChoreParent;
import org.apache.commons.io.IOUtils;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.hateoas.CollectionModel;

import java.io.IOException;
import java.util.ArrayList;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToJson;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.reset;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.springframework.http.HttpHeaders.ACCEPT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static wiremock.com.google.common.net.HttpHeaders.AUTHORIZATION;
import static wiremock.com.google.common.net.HttpHeaders.CONTENT_TYPE;

@SpringBootTest
@AutoConfigureWireMock(port = 0)
@Tag("unit")
class ChoreParentClientTest {

    @Value("${clients.chores-parents.path}")
    private String choreParentPath;

    @Autowired
    private ChoreParentClient client;

    @AfterEach
    void resetWireMock() {
        reset();
    }

    @Test
    void createChoreParentLink() throws IOException {
        // Arrange
        final String getAllChoreParentsResponse = IOUtils.toString(
                this.getClass().getResourceAsStream("/json/bb-chore-parent-crud/Get_Chore_Parent_Response.json"));
        final String expectedToken = "expected_token";
        final long expectedParentId = 123L;

        final long expectedChoreId = 456L;
        stubFor(post(urlEqualTo(choreParentPath))
                .withHeader(AUTHORIZATION, equalTo(expectedToken))
                .withHeader(CONTENT_TYPE, equalTo(APPLICATION_JSON_VALUE))
                .withRequestBody(
                        equalToJson("{\"parentId\": " + expectedParentId + ", \"choreId\": " + expectedChoreId + "}"))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .withBody(getAllChoreParentsResponse)));

        final ChoreParent expectedChoreParent = new ChoreParent();
        expectedChoreParent.setParentId(expectedParentId);
        expectedChoreParent.setChoreId(expectedChoreId);

        // Act
        client.createChoreParentLink(expectedToken, expectedChoreParent);

        // Assert
        verify(postRequestedFor(urlPathEqualTo(choreParentPath))
                .withHeader(AUTHORIZATION, equalTo(expectedToken))
                .withHeader(CONTENT_TYPE, equalTo(APPLICATION_JSON_VALUE))
                .withRequestBody(
                        equalToJson("{\"parentId\": " + expectedParentId + ", \"choreId\": " + expectedChoreId + "}")));
    }

    @Test
    void findByParentId() throws IOException {
        // Arrange
        final String getAllChoreParentsResponse = IOUtils
                .toString(this.getClass().getResourceAsStream("/json/bb-chore-parent-crud/Get_Chore_Parent_Response.json"));
        final String expectedToken = "expected_token";
        final long parentId = 123L;
        final String findByParentIdPath = choreParentPath + "/search/findByParentId";

        stubFor(get(urlPathEqualTo(findByParentIdPath))
                .withHeader(ACCEPT, equalTo(APPLICATION_JSON_VALUE))
                .withHeader(AUTHORIZATION, equalTo(expectedToken))
                .withQueryParam("parentId", equalTo(String.valueOf(parentId)))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .withBody(getAllChoreParentsResponse)));

        final ChoreParent expectedChoreParent = new ChoreParent();
        expectedChoreParent.setParentId(parentId);
        expectedChoreParent.setChoreId(456L);

        // Act
        final CollectionModel<ChoreParent> response = client.findByParentId(expectedToken, parentId);

        // Assert
        verify(getRequestedFor(urlPathEqualTo(findByParentIdPath))
                .withHeader(ACCEPT, equalTo(APPLICATION_JSON_VALUE))
                .withHeader(AUTHORIZATION, equalTo(expectedToken))
                .withQueryParam("parentId", equalTo(String.valueOf(parentId))));

        assertThat("Client returned a null response", response, notNullValue());
        assertThat("Client returned null content", response.getContent(), notNullValue());
        assertThat("Client returned empty content", response.getContent(), hasSize(greaterThan(0)));
        assertThat("Client returned content with more than one entity", response.getContent(), hasSize(1));
        final ArrayList responseAsList = new ArrayList<>(response.getContent());
        assertThat("Client returned wrong entity", responseAsList.get(0), IsEqual.equalTo(expectedChoreParent));
    }

}