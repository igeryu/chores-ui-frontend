package com.magipixel.chores.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.magipixel.chores.model.Chore;
import org.apache.commons.io.IOUtils;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.hateoas.CollectionModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToJson;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.reset;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static wiremock.com.google.common.net.HttpHeaders.ACCEPT;
import static wiremock.com.google.common.net.HttpHeaders.AUTHORIZATION;
import static wiremock.com.google.common.net.HttpHeaders.CONTENT_TYPE;
import static wiremock.com.google.common.primitives.Longs.asList;

@SpringBootTest
@AutoConfigureWireMock(port = 0)
@Tag("unit")
class ChoreClientTest {

    @Value("${clients.chores.path}")
    private String chorePath;

    @Autowired
    private ChoreClient client;

    @Autowired
    private ObjectMapper objectMapper;

    @AfterEach
    void resetWireMock() {
        reset();
    }

    @Test
    @DisplayName("createChore happy path")
    void createChore() throws IOException {
        // Arrange
        final String newChoreResponse =
                IOUtils.toString(this.getClass().getResourceAsStream("/json/bb-chore-crud/New_Chore_Response.json"));
        final String userJson =
                IOUtils.toString(this.getClass().getResourceAsStream("/json/bb-chore-crud/New_Chore_Request.json"));
        final String expectedToken = "expected_token";

        stubFor(post(urlMatching(chorePath))
                .withHeader(ACCEPT, equalTo(APPLICATION_JSON_VALUE))
                .withHeader(CONTENT_TYPE, equalTo(APPLICATION_JSON_VALUE))
                .withHeader(AUTHORIZATION, equalTo(expectedToken))
                .withRequestBody(equalToJson(userJson))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .withBody(newChoreResponse)));

        final Chore user = objectMapper.readValue(userJson, Chore.class);

        // Act
        final Chore choreResponse = client.createChore(expectedToken, user);

        // Assert
        verify(postRequestedFor(urlMatching(chorePath))
                .withHeader(ACCEPT, equalTo(APPLICATION_JSON_VALUE))
                .withHeader(CONTENT_TYPE, equalTo(APPLICATION_JSON_VALUE))
                .withHeader(AUTHORIZATION, equalTo(expectedToken))
                .withRequestBody(equalToJson(userJson)));

        final Chore expectedChore = new Chore();
        expectedChore.setId(123L);
        expectedChore.setName("can_be_anything");
        assertThat(choreResponse, IsEqual.equalTo(expectedChore));
    }

    @Test
    void findAllByIds() throws IOException {
        // Arrange
        final String getAllChoresResponse = IOUtils
                .toString(this.getClass().getResourceAsStream("/json/bb-chore-crud/Get_All_Chores_Response.json"));
        final String expectedToken = "expected_token";

        final List<Long> choreIds = asList(1L, 2L);
        final String findByIdsPath = chorePath + "/search/findByIdIn";
        stubFor(get(urlPathEqualTo(findByIdsPath))
                .withHeader(ACCEPT, equalTo(APPLICATION_JSON_VALUE))
                .withHeader(AUTHORIZATION, equalTo(expectedToken))
                .withQueryParam("ids", equalTo("1"))
                .withQueryParam("ids", equalTo("2"))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .withBody(getAllChoresResponse)));

        final Chore expectedChore = new Chore();
        expectedChore.setId(1L);
        expectedChore.setName("Chore A");

        // Act
        final CollectionModel<Chore> response = client.findAllByIds(expectedToken, choreIds);

        // Assert
        verify(getRequestedFor(urlPathEqualTo(findByIdsPath))
                .withHeader(ACCEPT, equalTo(APPLICATION_JSON_VALUE))
                .withHeader(AUTHORIZATION, equalTo(expectedToken))
                .withQueryParam("ids", equalTo("1"))
                .withQueryParam("ids", equalTo("2")));

        assertThat("Client returned a null response", response, notNullValue());
        assertThat("Client returned null content", response.getContent(), notNullValue());
        assertThat("Client returned empty content", response.getContent(), hasSize(greaterThan(0)));
        assertThat("Client returned content with only one entity", response.getContent(), hasSize(greaterThan(1)));
        assertThat("Client returned content with more than one entity", response.getContent(), hasSize(2));
        final ArrayList responseAsList = new ArrayList<>(response.getContent());
        assertThat("Client returned wrong entity", responseAsList.get(0), IsEqual.equalTo(expectedChore));
    }

}