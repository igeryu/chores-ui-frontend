package com.magipixel.chores.client;

import com.magipixel.chores.model.ChoreChild;
import org.apache.commons.io.IOUtils;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.hateoas.CollectionModel;

import java.io.IOException;
import java.util.ArrayList;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToJson;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.reset;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static wiremock.com.google.common.net.HttpHeaders.AUTHORIZATION;
import static wiremock.com.google.common.net.HttpHeaders.CONTENT_TYPE;

@SpringBootTest
@AutoConfigureWireMock(port = 0)
@Tag("unit")
class ChoreChildClientTest {

    @Value("${clients.chores-children.path}")
    private String choreChildPath;

    @Autowired
    private ChoreChildClient client;

    @AfterEach
    void resetWireMock() {
        reset();
    }

    @Test
    void createChoreChildLink() throws IOException {
        // Arrange
        final String getAllChoreChildrenResponse = IOUtils.toString(
                this.getClass().getResourceAsStream("/json/bb-chore-child-crud/Get_Chore_Child_Response.json"));
        final String expectedToken = "expected_token";
        final long expectedChildId = 123L;

        final long expectedChoreId = 456L;
        stubFor(post(urlEqualTo(choreChildPath))
                .withHeader(AUTHORIZATION, equalTo(expectedToken))
                .withHeader(CONTENT_TYPE, equalTo(APPLICATION_JSON_VALUE))
                .withRequestBody(
                        equalToJson("{\"childId\": " + expectedChildId + ", \"choreId\": " + expectedChoreId + "}"))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .withBody(getAllChoreChildrenResponse)));

        final ChoreChild inputChildParent = new ChoreChild(expectedChildId, expectedChoreId);

        // Act
        client.createChoreChildLink(expectedToken, inputChildParent);

        // Assert
        verify(postRequestedFor(urlPathEqualTo(choreChildPath))
                .withHeader(AUTHORIZATION, equalTo(expectedToken))
                .withHeader(CONTENT_TYPE, equalTo(APPLICATION_JSON_VALUE))
                .withRequestBody(
                        equalToJson("{\"childId\": " + expectedChildId + ", \"choreId\": " + expectedChoreId + "}")));
    }

    @Test
    void findByChildId() throws IOException {
        // Arrange
        final String getAllChoreChildResponse = IOUtils.toString(
                this.getClass().getResourceAsStream("/json/bb-chore-child-crud/Get_Chore_Child_Response.json"));
        final String expectedToken = "expected_token";
        final long childId = 456L;
        final String findByChildIdPath = choreChildPath + "/search/findByChildId";

        stubFor(get(urlPathEqualTo(findByChildIdPath))
                .withHeader(CONTENT_TYPE, equalTo(APPLICATION_JSON_VALUE))
                .withHeader(AUTHORIZATION, equalTo(expectedToken))
                .withQueryParam("childId", equalTo(String.valueOf(childId)))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .withBody(getAllChoreChildResponse)));

        final ChoreChild expectedChoreChild = new ChoreChild(childId, 123L);

        // Act
        final CollectionModel<ChoreChild> response = client.findByChildId(expectedToken, childId);

        // Assert
        verify(getRequestedFor(urlPathEqualTo(findByChildIdPath))
                .withHeader(CONTENT_TYPE, equalTo(APPLICATION_JSON_VALUE))
                .withHeader(AUTHORIZATION, equalTo(expectedToken))
                .withQueryParam("childId", equalTo(String.valueOf(childId))));

        assertThat("Client returned a null response", response, notNullValue());
        assertThat("Client returned null content", response.getContent(), notNullValue());
        assertThat("Client returned empty content", response.getContent(), hasSize(greaterThan(0)));
        assertThat("Client returned content with more than one entity", response.getContent(), hasSize(1));
        final ArrayList<ChoreChild> responseAsList = new ArrayList<>(response.getContent());
        assertThat("Client returned wrong entity", responseAsList.get(0), IsEqual.equalTo(expectedChoreChild));
    }

}