package com.magipixel.chores.controller;

import com.magipixel.chores.exception.DuplicateChoreException;
import com.magipixel.chores.exception.EntityNotFoundException;
import com.magipixel.chores.exception.ErrorResponse;
import com.magipixel.chores.model.Chore;
import com.magipixel.chores.service.ChoreService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/chores")
@RequiredArgsConstructor
@Slf4j
public class ChoreController {
    private static final String MSG_MISSING_AUTH =
            "Not Authorized: Request missing 'Authorization' header";

    private final ChoreService service;

    @PreAuthorize("hasRole('PARENT')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createChore(@RequestHeader("Authorization") final String token,
                            final Principal principal,
                            @RequestBody final Chore chore) throws DuplicateChoreException {
        if (token == null) {
            throw new AuthenticationCredentialsNotFoundException(MSG_MISSING_AUTH);
        }
        final String parentEmail = principal != null ? principal.getName() : null;
        log.trace("Creating child for Parent: {}, name: {}", parentEmail, chore.getName());
        this.service.createChore(token, parentEmail, chore);
    }

    @GetMapping
    public Iterable<Chore> getAllChores(@RequestHeader("Authorization") final String token,
                                        final Principal principal) {
        if (token == null) {
            throw new AuthenticationCredentialsNotFoundException(MSG_MISSING_AUTH);
        }
        final String parentEmail = principal != null ? principal.getName() : null;
        log.trace("Getting all chores for parent: {}", parentEmail);
        return this.service.getAllChores(token, parentEmail);
    }

    @ExceptionHandler(DuplicateChoreException.class)
    public ResponseEntity<ErrorResponse> handleDuplicateChore(final DuplicateChoreException ex) {
        log.warn("Returning 409 Conflict: {}", ex.getMessage());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse(ex.getMessage()));
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleParentNotFound(final EntityNotFoundException ex) {
        log.warn("Returning 400 Bad Request: {}", ex.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse("Parent not found"));
    }
}
