package com.magipixel.chores.controller;

import com.magipixel.chores.exception.DuplicateAccountException;
import com.magipixel.chores.exception.ErrorResponse;
import com.magipixel.chores.model.User;
import com.magipixel.chores.service.CredentialsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/credentials/users")
@RequiredArgsConstructor
@Slf4j
public class CredentialsController {

    private final CredentialsService service;

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public void createCredentials(@RequestBody final User user) throws DuplicateAccountException {
        log.debug("Creating credentials, username: {}", user.getUsername());
        log.trace("Creating credentials, username: {}, password {}", user.getUsername(), user.getPassword());
        this.service.createCredentials(user);
    }

    @ExceptionHandler(DuplicateAccountException.class)
    public ResponseEntity<ErrorResponse> handleDuplicateAccountException(DuplicateAccountException ex) {
        log.warn("Returning 409 Conflict: {}", ex.getMessage());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse(ex.getMessage()));
    }
}
