package com.magipixel.chores.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.magipixel.chores.exception.DuplicateChildException;
import com.magipixel.chores.exception.EntityNotFoundException;
import com.magipixel.chores.exception.ErrorResponse;
import com.magipixel.chores.model.Child;
import com.magipixel.chores.model.Chore;
import com.magipixel.chores.service.ChildService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Collection;

@RestController
@RequestMapping("/children")
@RequiredArgsConstructor
@Slf4j
public class ChildController {
    private static final String MSG_MISSING_AUTH =
            "Not Authorized: Request missing 'Authorization' header";

    private final ChildService service;
    private final ObjectMapper mapper;

    @PreAuthorize("hasRole('PARENT')")
    @GetMapping
    public Collection<Child> getAllChildren(
            @RequestHeader(
                    name = "Authorization",
                    required = false) final String token,
            final Principal principal) throws JsonProcessingException {
        this.verifyToken(token);
        final String parentEmail = principal != null ? principal.getName() : null;

        log.trace("Getting all children for parent: {}", parentEmail);
        final Collection<Child> allChildren = service.getAllChildren(token, parentEmail);

        log.trace("Returning all children: {}", mapper.writeValueAsString(allChildren));
        return allChildren;
    }

    @GetMapping("{id}")
    public Child getChild(
            @RequestHeader(
                    name = "Authorization",
                    required = false) final String token,
            @PathVariable final long id) {
        this.verifyToken(token);
        log.trace("Getting child for id: {}", id);
        return this.service.getChild(token, id);
    }

    @PreAuthorize("hasRole('PARENT')")
    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public void createChild(
            @RequestHeader(
                    name = "Authorization",
                    required = false) final String token,
            final Principal principal,
            @RequestBody final Child child) throws DuplicateChildException {
        this.verifyToken(token);
        final String parentEmail = principal != null ? principal.getName() : null;
        log.trace("Creating child for Parent: {}, firstName: {}, email {}",
                parentEmail, child.getFirstName(), child.getEmail());
        this.service.createChild(token, parentEmail, child);
    }

    @PutMapping("{childId}/chores")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void assignChore(@RequestHeader("Authorization") final String token,
                            @PathVariable final long childId,
                            @RequestBody final Chore chore) {
        this.verifyToken(token);
        log.trace("Assigning chore ({}) to child: {}", chore, childId);
        this.service.assignChore(token, childId, chore);
    }

    @GetMapping("{childId}/chores")
    public Collection<Chore> getChildChores(@RequestHeader("Authorization") final String token,
                                            @PathVariable final long childId) {
        this.verifyToken(token);
        final Collection<Chore> childChores = this.service.getChildChores(token, childId);
        log.trace("Getting chores ({}) for child: {}", childChores, childId);
        return childChores;
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<String> handleAccessDenied(AccessDeniedException ex) {
        log.trace("Returning 403 Forbidden: {}", ex.getMessage());
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(ex.getMessage());
    }

    @ExceptionHandler(AuthenticationCredentialsNotFoundException.class)
    public ResponseEntity<String> handleUnauthorized(AuthenticationCredentialsNotFoundException ex) {
        log.trace("Returning 401 Unauthorized: {}", ex.getMessage());
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(ex.getMessage());
    }

    @ExceptionHandler(DuplicateChildException.class)
    public ResponseEntity<ErrorResponse> handleDuplicateChildException(final DuplicateChildException ex) {
        log.warn("Returning 409 Conflict: {}", ex.getMessage());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse(ex.getMessage()));
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleEntityNotFoundException(final EntityNotFoundException ex) {
        log.warn("Returning 404 Not Found: {}", ex.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(ex.getMessage()));
    }

    private void verifyToken(final String token) {
        if (token == null) {
            log.warn("Authorization header missing");
            throw new AuthenticationCredentialsNotFoundException(MSG_MISSING_AUTH);
        }
    }
}
