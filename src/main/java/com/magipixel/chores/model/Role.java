package com.magipixel.chores.model;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Role {
    PARENT("PARENT");

    private final String value;

    Role(final String value) {
        this.value = value;
    }

    @JsonValue
    @Override
    public String toString() {
        return "ROLE_" + this.value;
    }
}