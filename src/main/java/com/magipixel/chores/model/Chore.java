package com.magipixel.chores.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Map;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public final class Chore {
    private Long id;
    private String name;

    @JsonProperty("_links")
    @SuppressWarnings("unchecked")
    public void unpackIdFromLinks(final Map<String, Object> links) {
        final String uri = ((Map<String, String>) links.get("self")).get("href");
        final int idStartIndex = uri.lastIndexOf('/') + 1;
        final String idStr = uri.substring(idStartIndex);
        this.id = Long.parseLong(idStr);
    }
}
