package com.magipixel.chores.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public final class ChildParent {
    private long parentId;
    private long childId;
}
