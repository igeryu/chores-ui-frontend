package com.magipixel.chores.exception;

public class DuplicateChoreException extends Exception {
    public DuplicateChoreException(String message) {
        super(message);
    }
}
