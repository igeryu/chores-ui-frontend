package com.magipixel.chores.exception;

public class DuplicateChildException extends Exception {
    public DuplicateChildException(String message) {
        super(message);
    }
}
