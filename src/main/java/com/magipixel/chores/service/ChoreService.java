package com.magipixel.chores.service;

import com.magipixel.chores.client.ChoreClient;
import com.magipixel.chores.client.ChoreParentClient;
import com.magipixel.chores.client.CredentialsClient;
import com.magipixel.chores.exception.DuplicateChoreException;
import com.magipixel.chores.exception.EntityNotFoundException;
import com.magipixel.chores.model.Chore;
import com.magipixel.chores.model.ChoreParent;
import com.magipixel.chores.model.User;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class ChoreService {
    private final CredentialsClient credentialsClient;
    private final ChoreClient choreClient;
    private final ChoreParentClient choreParentClient;

    public void createChore(final String token,
                            final String parentEmail,
                            final Chore chore) throws DuplicateChoreException {
        log.trace("Looking up parent by email: {}", parentEmail);
        final Optional<User> parentOptional = this.credentialsClient.findByEmail(token, parentEmail);
        if (!parentOptional.isPresent()) {
            throw new EntityNotFoundException();
        }
        final User parent = parentOptional.get();

        final Chore createdChore;
        try {
            log.trace("Creating Chore, name: {},", chore.getName());
            createdChore = this.choreClient.createChore(token, chore);
        } catch (FeignException.FeignClientException ex) {
            throw new DuplicateChoreException("Chore already exists: " + chore.getName());
        }

        final ChoreParent choreParent = new ChoreParent();
        final long parentId = parent.getId();
        choreParent.setParentId(parentId);
        final long choreId = createdChore.getId();
        choreParent.setChoreId(choreId);

        log.trace("Associating choreId {}, with parentId {}", choreId, parentId);
        this.choreParentClient.createChoreParentLink(token, choreParent);
    }

    public Collection<Chore> getAllChores(final String token,
                                          final String email) {
        log.trace("Looking up parent by email: {}", email);
        final Optional<User> parentOptional = this.credentialsClient.findByEmail(token, email);
        if (!parentOptional.isPresent()) {
            throw new EntityNotFoundException();
        }
        final long parentId = parentOptional.get().getId();
        log.trace("Getting list of Chore IDs for parentId: {}", parentId);
        final Collection<ChoreParent> choreParents =
                this.choreParentClient.findByParentId(token, parentId).getContent();

        if (choreParents.size() > 0) {
            final Collection<Long> choreIds = new ArrayList<>();
            for (final ChoreParent choreParent : choreParents) {
                choreIds.add(choreParent.getChoreId());
            }
            log.trace("Getting list of Chores with IDs: {}", choreIds);
            return this.choreClient.findAllByIds(token, choreIds).getContent();
        } else {
            return Collections.emptyList();
        }
    }
}
