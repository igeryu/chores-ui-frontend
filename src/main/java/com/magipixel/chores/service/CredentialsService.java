package com.magipixel.chores.service;

import com.magipixel.chores.client.CredentialsClient;
import com.magipixel.chores.exception.DuplicateAccountException;
import com.magipixel.chores.model.Role;
import com.magipixel.chores.model.User;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CredentialsService {
    private final CredentialsClient client;

    public void createCredentials(final User user) throws DuplicateAccountException {
        user.setRole(Role.PARENT);
        try {
            this.client.createCredentials(user);
        } catch (FeignException.Conflict conflict) {
            throw new DuplicateAccountException("Account already exists: " + user.getUsername());
        }
    }
}
