package com.magipixel.chores.service;

import com.magipixel.chores.client.ChildClient;
import com.magipixel.chores.client.ChildParentClient;
import com.magipixel.chores.client.ChoreChildClient;
import com.magipixel.chores.client.ChoreClient;
import com.magipixel.chores.client.CredentialsClient;
import com.magipixel.chores.exception.DuplicateChildException;
import com.magipixel.chores.exception.EntityNotFoundException;
import com.magipixel.chores.model.Child;
import com.magipixel.chores.model.ChildParent;
import com.magipixel.chores.model.Chore;
import com.magipixel.chores.model.ChoreChild;
import com.magipixel.chores.model.User;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class ChildService {
    private final ChildClient childClient;
    private final CredentialsClient credentialsClient;
    private final ChildParentClient childParentClient;
    private final ChoreChildClient choreChildClient;
    private final ChoreClient choreClient;

    public Collection<Child> getAllChildren(final String token,
                                            final String parentEmail) {
        log.trace("Looking up parent by email: {}", parentEmail);
        final Optional<User> parentOptional = this.credentialsClient.findByEmail(token, parentEmail);

        if (!parentOptional.isPresent()) {
            throw new EntityNotFoundException();
        }

        final long parentId = parentOptional.get().getId();
        log.trace("Getting list of Child IDs for parentId: {}", parentId);
        final ArrayList<ChildParent> childParents = new ArrayList<>(
                this.childParentClient.findByParentId(token, parentId).getContent());

        if (childParents.isEmpty()) {
            log.trace("No children for parent: {}", parentEmail);
            return Collections.emptyList();
        }

        final List<Long> childIds = new ArrayList<>();
        for (final ChildParent childParent : childParents) {
            childIds.add(childParent.getChildId());
        }

        log.trace("Getting list of Children with IDs: {}", childIds);
        return this.childClient.findAllByIds(token, childIds).getContent();
    }

    public void createChild(final String token,
                            final String parentEmail,
                            final Child child) throws DuplicateChildException {
        log.trace("Looking up parent by email: {}", parentEmail);
        final Optional<User> parentOptional = credentialsClient.findByEmail(token, parentEmail);
        if (!parentOptional.isPresent()) {
            throw new EntityNotFoundException();
        }
        final User parent = parentOptional.get();
        log.trace("Parent: {}", parent);

        final long childId;
        try {
            log.trace("Creating child, firstName: {}, email {}", child.getFirstName(), child.getEmail());
            childId = this.childClient.createChild(token, child).getId();
        } catch (FeignException.Conflict conflict) {
            throw new DuplicateChildException("Child already exists: " + child.getEmail());
        }

        final ChildParent childParent = new ChildParent();
        final long parentId = parent.getId();
        childParent.setParentId(parentId);
        childParent.setChildId(childId);
        log.trace("Associating childId {}, with parentId {}", childId, parentId);
        childParentClient.createChildParentLink(token, childParent);
    }

    public Child getChild(final String token,
                          final long id) {
        final Optional<Child> childOptional = childClient.getChild(token, id);
        if (childOptional.isPresent()) {
            return childOptional.get();
        }

        throw new EntityNotFoundException();
    }

    public void assignChore(final String token,
                            final long childId,
                            final Chore chore) {
        log.trace("Looking up child by id: {}", childId);
        final Optional<Child> childOptional = this.childClient.getChild(token, childId);

        if (childOptional.isPresent()) {
            log.trace("Child found: {}.  Assigning to chore: {}", childOptional.get(), chore);
            this.choreChildClient.createChoreChildLink(token, new ChoreChild(childId, chore.getId()));
        } else {
            log.warn("Child not found for id: {}", childId);
            throw new EntityNotFoundException();
        }
    }

    public Collection<Chore> getChildChores(final String token,
                                            final long childId) {
        final Optional<Child> childOptional = this.childClient.getChild(token, childId);
        if (childOptional.isPresent()) {
            final Collection<ChoreChild> choreChildren =
                    this.choreChildClient.findByChildId(token, childId).getContent();

            if (choreChildren.isEmpty()) {
                log.trace("No chores for child: {}", childId);
                return Collections.emptyList();
            }

            final List<Long> choreIds = new ArrayList<>();
            for (final ChoreChild choreChild : choreChildren) {
                choreIds.add(choreChild.getChoreId());
            }

            return choreClient.findAllByIds(token, choreIds).getContent();
        } else {
            throw new EntityNotFoundException();
        }
    }
}
