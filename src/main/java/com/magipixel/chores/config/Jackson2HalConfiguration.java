package com.magipixel.chores.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.mediatype.hal.Jackson2HalModule;

@Configuration
class Jackson2HalConfiguration {

    @Bean
    public Jackson2HalModule jacksonHalModule() {
        return new Jackson2HalModule();
    }
}
