package com.magipixel.chores.client;

import com.magipixel.chores.model.Child;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.hateoas.CollectionModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@FeignClient(
        name = "childClient",
        url = "${clients.children.hostname}",
        path = "${clients.children.path}"
)
public interface ChildClient {
    @GetMapping(
            produces = APPLICATION_JSON_VALUE
    )
    CollectionModel<Child> getAllChildren(@RequestHeader("Authorization") final String token);

    @PostMapping(
            produces = APPLICATION_JSON_VALUE,
            consumes = APPLICATION_JSON_VALUE
    )
    Child createChild(@RequestHeader("Authorization") final String token,
                      final Child child);

    @GetMapping(
            path = "/search/findByIdIn",
            produces = APPLICATION_JSON_VALUE
    )
    CollectionModel<Child> findAllByIds(@RequestHeader("Authorization") final String token,
                                        @RequestParam final List<Long> ids);

    @GetMapping(
            path = "{id}",
            produces = APPLICATION_JSON_VALUE
    )
    Optional<Child> getChild(@RequestHeader("Authorization") String token,
                             @PathVariable long id);
}
