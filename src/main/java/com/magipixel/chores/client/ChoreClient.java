package com.magipixel.chores.client;

import com.magipixel.chores.model.Chore;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.hateoas.CollectionModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@FeignClient(
        name = "choreClient",
        url = "${clients.chores.hostname}",
        path = "${clients.chores.path}"
)
public interface ChoreClient {

    @PostMapping(
            produces = APPLICATION_JSON_VALUE,
            consumes = APPLICATION_JSON_VALUE
    )
    Chore createChore(@RequestHeader("Authorization") String token,
                      @RequestBody Chore chore);

    @GetMapping(
            path = "/search/findByIdIn",
            produces = APPLICATION_JSON_VALUE
    )
    CollectionModel<Chore> findAllByIds(@RequestHeader("Authorization") String token,
                                        @RequestParam Iterable<Long> ids);
}
