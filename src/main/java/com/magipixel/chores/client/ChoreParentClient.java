package com.magipixel.chores.client;

import com.magipixel.chores.model.ChoreParent;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.hateoas.CollectionModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@FeignClient(
        name = "choreParentClient",
        url = "${clients.chores-parents.hostname}",
        path = "${clients.chores-parents.path}"
)
public interface ChoreParentClient {
    @PostMapping
    void createChoreParentLink(@RequestHeader("Authorization") String token,
                               @RequestBody ChoreParent choreParent);

    @GetMapping(
            path = "/search/findByParentId",
            produces = APPLICATION_JSON_VALUE
    )
    CollectionModel<ChoreParent> findByParentId(@RequestHeader("Authorization") String token,
                                                @RequestParam long parentId);
}
