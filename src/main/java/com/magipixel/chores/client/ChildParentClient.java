package com.magipixel.chores.client;

import com.magipixel.chores.model.ChildParent;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.hateoas.CollectionModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@FeignClient(
        name = "childParentClient",
        url = "${clients.children-parents.hostname}",
        path = "${clients.children-parents.path}"
)
public interface ChildParentClient {
    @GetMapping(
            path = "/search/findByParentId",
            produces = APPLICATION_JSON_VALUE
    )
    CollectionModel<ChildParent> findByParentId(@RequestHeader("Authorization") final String token,
                                                @RequestParam final long parentId);

    @PostMapping
    void createChildParentLink(@RequestHeader("Authorization") final String token,
                               final ChildParent childParent);
}
