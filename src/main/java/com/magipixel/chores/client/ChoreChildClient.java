package com.magipixel.chores.client;

import com.magipixel.chores.model.ChoreChild;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.hateoas.CollectionModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@FeignClient(
        name = "choreChildClient",
        url = "${clients.chores-children.hostname}",
        path = "${clients.chores-children.path}"
)
public interface ChoreChildClient {
    @PostMapping
    void createChoreChildLink(@RequestHeader("Authorization") String token,
                              ChoreChild choreChild);

    @GetMapping(
            path = "/search/findByChildId",
            consumes = APPLICATION_JSON_VALUE
    )
    CollectionModel<ChoreChild> findByChildId(@RequestHeader("Authorization") String token,
                                              @RequestParam long childId);
}
