package com.magipixel.chores.client;

import com.magipixel.chores.model.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@FeignClient(
        name = "credentialsClient",
        url = "${clients.credentials.hostname}",
        path = "${clients.credentials.path}"
)
public interface CredentialsClient {
    @PostMapping
    void createCredentials(final User user);

    @GetMapping("/search/findByUsername")
    Optional<User> findByEmail(@RequestHeader("Authorization") final String token,
                               @RequestParam("username") final String parentEmail);
}
